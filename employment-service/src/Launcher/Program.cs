using Employment.Api;
using Employment.Core;
using Employment.Database;
using Employment.Server;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Serilog;

using System;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .Enrich.FromLogContext()
    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{NewLine}\t{Exception}{NewLine}", theme: Serilog.Sinks.SystemConsole.Themes.AnsiConsoleTheme.Code)
    .CreateBootstrapLogger();

Log.Information("Starting up");

try {
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((context, services, configuration) =>
        configuration
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{NewLine}\t{Exception}{NewLine}", theme: Serilog.Sinks.SystemConsole.Themes.AnsiConsoleTheme.Code)
            .ReadFrom.Services(services)
            .ReadFrom.Configuration(context.Configuration)
        );
    builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
        config.AddJsonFile("appsettings.local.json", optional: true, reloadOnChange: true)
    );

    builder.Services.AddRazorPages();
    builder.Services.AddControllers();

    builder.Services.AddCore();
    builder.Services.AddDatabase(builder.Configuration);
    builder.Services.AddApi();
    builder.Services.AddServer();

    var app = builder.Build();

    if (!app.Environment.IsDevelopment()) {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
    }
    app.UseHttpsRedirection();

    app.UseStaticFiles();

    app.UseRouting();

    app.UseAuthentication();
    app.UseServer();
    app.UseAuthorization();
    app.UseDatabase(app.Configuration);
    app.UseApi(app.Environment);

    app.UseEndpoints(configure =>
        configure
            .MapApiEndpoints()
            .MapServerEndpoints()
    );

    app.Run();
} catch (Exception ex) {
    Log.Fatal(ex, "Unhandled exception");
} finally {
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}