﻿// using Microsoft.AspNetCore.Builder;
// using Microsoft.AspNetCore.Hosting;
// using Microsoft.AspNetCore.Http;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.DependencyInjection;
// using Microsoft.Extensions.Hosting;

// using Serilog;

// using Users.Database;
// using Users.Server;

// namespace Users.Launcher;
// public class Startup {
//     private readonly ILogger logger;
//     private readonly IConfiguration configuration;

//     public Startup(IConfiguration configuration) {
//         this.logger = Log.Logger.ForContext<Startup>();
//         this.configuration = configuration;
//     }

//     public void ConfigureServices(IServiceCollection services) {
//         logger.Information("Configuring services... ");
//         services.AddAuthentication();

//         services.AddCore(configuration);
//         logger.Information("Added Core");
//         services.AddDatabase(configuration);
//         logger.Information("Added Database");
//         services.AddApi(configuration);
//         logger.Information("Added Api");
//         services.AddServer(configuration);
//         logger.Information("Added Server");

//         logger.Information("Configuring services... COMPLETED!");
//     }

//     public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
//         logger.Information("Configuring app...");
//         app.UseCookiePolicy(new CookiePolicyOptions { MinimumSameSitePolicy = SameSiteMode.Lax });

//         if (env.IsDevelopment()) {
//             app.UseDeveloperExceptionPage();
//         }

//         app.UseHttpsRedirection();

//         app.UseRouting();

//         app.UseCore(env, configuration);
//         logger.Information("Used Core");
//         app.UseDatabase(env, configuration);
//         logger.Information("Used Database");
//         app.UseApi(env, configuration);
//         logger.Information("Used Api");
//         app.UseServer(env, configuration);
//         logger.Information("Used Server");

//         app.UseAuthorization();

//         app.UseEndpoints(endpoints => {
//             endpoints.MapControllers();
//         });

//         logger.Information("Configuring app... COMPLETED!");
//     }
// }
