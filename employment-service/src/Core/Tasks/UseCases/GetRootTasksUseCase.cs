using Employment.Core.Abstraction;
using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employment.Core.Tasks.UseCases;
public sealed class GetRootTasksUseCase : IUseCase<GetRootTasksUseCase.IInputPort, GetRootTasksUseCase.IOutputPort> {
    private readonly ITasksRepository tasksRepository;

    public GetRootTasksUseCase(ITasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        var tasks = await tasksRepository.GetManyAsync(task => task.Parent == null);

        outputPort.Found(tasks);
    }

    public interface IInputPort : Abstraction.IInputPort {
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Found(IEnumerable<TaskEntity> tasks);
    }
}
