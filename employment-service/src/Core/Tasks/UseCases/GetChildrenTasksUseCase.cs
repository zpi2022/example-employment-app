using Employment.Core.Abstraction;
using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employment.Core.Tasks.UseCases;
public sealed class GetChildrenTasksUseCase : IUseCase<GetChildrenTasksUseCase.IInputPort, GetChildrenTasksUseCase.IOutputPort> {
    private readonly ITasksRepository tasksRepository;

    public GetChildrenTasksUseCase(ITasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        var task = await tasksRepository.GetOneAsync(inputPort.TaskIdentifier);

        if (task == null) {
            outputPort.NotFound($"There is no task with identifier {inputPort.TaskIdentifier}");
            return;
        }

        outputPort.Found(task.Children);
    }

    public interface IInputPort : Abstraction.IInputPort {
        long TaskIdentifier { get; }
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Found(IEnumerable<TaskEntity> tasks);
        void NotFound(string message);
    }
}
