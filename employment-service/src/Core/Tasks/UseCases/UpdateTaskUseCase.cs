using Employment.Core.Abstraction;
using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.Repositories;
using Employment.Core.Users.Entities;
using Employment.Core.Users.Repositories;

using System;
using System.Threading.Tasks;

namespace Employment.Core.Tasks.UseCases;
public sealed class UpdateTaskUseCase : IUseCase<UpdateTaskUseCase.IInputPort, UpdateTaskUseCase.IOutputPort> {
    private readonly ITasksRepository tasksRepository;
    private readonly IUsersRepository usersRepository;

    public UpdateTaskUseCase(ITasksRepository tasksRepository, IUsersRepository usersRepository) {
        this.tasksRepository = tasksRepository;
        this.usersRepository = usersRepository;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        var task = await tasksRepository.GetOneAsync(inputPort.TaskIdentifier);
        if (task == null) {
            outputPort.NotFound($"There is no {nameof(Task)} with id '{inputPort.TaskIdentifier}'");
            return;
        }

        User? user = null;
        if (inputPort.AssignedUserIdentifier != null) {
            user = await usersRepository.GetOneAsync((long)inputPort.AssignedUserIdentifier);
            if (user == null) {
                outputPort.NotFound($"There is no {nameof(User)} with id '{inputPort.AssignedUserIdentifier}'");
                return;
            }
        }

        TaskEntity? parent = null;
        if (inputPort.ParentIdentifier != null) {
            parent = await tasksRepository.GetOneAsync((long)inputPort.ParentIdentifier);
            if (parent == null) {
                outputPort.NotFound($"There is no {nameof(Task)} with id '{inputPort.ParentIdentifier}'");
                return;
            }
        }

        task.Name = inputPort.Name;
        task.Description = inputPort.Description;
        task.Priority = inputPort.Priority;
        task.State = inputPort.State;
        task.Parent = parent;
        task.AssignedUser = user;

        await tasksRepository.UpdateAsync(task);

        outputPort.Created(task);
    }

    public interface IInputPort : Abstraction.IInputPort {
        long TaskIdentifier { get; }
        string Name { get; }
        string Description { get; }
        int Priority { get; }
        TaskState State { get; }
        long? ParentIdentifier { get; }
        long? AssignedUserIdentifier { get; }
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Created(TaskEntity task);
        void NotFound(string message);
    }
}
