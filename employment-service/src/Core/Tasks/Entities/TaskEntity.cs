using Employment.Core.Users.Entities;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Employment.Core.Tasks.Entities;
public class TaskEntity {
    public long Identifier { get; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Priority { get; set; }
    public TaskState State { get; set; }

    public virtual ICollection<TaskEntity> Children { get; set; } = new Collection<TaskEntity>();
    public virtual TaskEntity? Parent { get; set; }
    public virtual User? AssignedUser { get; set; }
}
