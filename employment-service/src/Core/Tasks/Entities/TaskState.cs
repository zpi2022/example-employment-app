namespace Employment.Core.Tasks.Entities;
public enum TaskState {
    New,
    Planned,
    InProgress,
    Completed,
    Blocked,
    Cancelled,
}
