using Employment.Core.Abstraction;

using Employment.Core.Tasks.Entities;

namespace Employment.Core.Tasks.Repositories;
public interface ITasksRepository : IAsyncRepository<long, TaskEntity> {
}
