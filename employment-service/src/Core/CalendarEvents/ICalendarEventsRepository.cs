using Employment.Core.Abstraction;
using Employment.Core.CalendarEvents.Entities;

namespace Employment.Core.CalendarEvents.Repositories;
public interface ICalendarEventsRepository : IAsyncRepository<long, CalendarEvent> {
}
