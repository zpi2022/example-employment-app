using Employment.Core.Users.Entities;

using System;

namespace Employment.Core.CalendarEvents.Entities;
public class CalendarEvent {
    public long Identifier { get; }
    public DateOnly StartDate { get; set; }
    public DateOnly EndDate { get; set; }
    public CalendarEventStatus Status { get; set; }

    public virtual User User { get; set; }
}
