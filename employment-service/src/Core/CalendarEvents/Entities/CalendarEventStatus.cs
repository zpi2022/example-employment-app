namespace Employment.Core.CalendarEvents.Entities;
public enum CalendarEventStatus {
    PENDING,
    REJECTED,
    ACCEPTED,
    CANCELLED
}