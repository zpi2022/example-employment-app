using Employment.Core.Abstraction;
using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employment.Core.CalendarEvents.UseCases;
public sealed class GetCalendarEventsUseCase : IUseCase<GetCalendarEventsUseCase.IInputPort, GetCalendarEventsUseCase.IOutputPort> {
    private readonly ICalendarEventsRepository eventsRepository;

    public GetCalendarEventsUseCase(ICalendarEventsRepository eventsRepository) {
        this.eventsRepository = eventsRepository;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        var events = await eventsRepository.GetManyAsync(calendarEvent => inputPort.UserIdentifier == null || calendarEvent.User.Id == inputPort.UserIdentifier);

        var filtered = events.Where(calendarEvent => {
            var result = true;
            if (inputPort.MinDate.HasValue) {
                result = result && calendarEvent.EndDate >= inputPort.MinDate.Value!;
            }
            if (inputPort.MaxDate.HasValue) {
                result = result && calendarEvent.StartDate <= inputPort.MaxDate.Value!;
            }
            if (inputPort.MinDate.HasValue && inputPort.MaxDate.HasValue) {
                result = result || inputPort.MinDate.Value! < calendarEvent.EndDate && calendarEvent.StartDate < inputPort.MaxDate.Value!;
            }
            return result;
        });

        outputPort.Found(filtered);
    }

    public interface IInputPort : Abstraction.IInputPort {
        long? UserIdentifier { get; }
        DateOnly? MinDate { get; }
        DateOnly? MaxDate { get; }
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Found(IEnumerable<CalendarEvent> calendarEvents);
    }
}
