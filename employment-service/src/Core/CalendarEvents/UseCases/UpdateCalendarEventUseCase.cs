using Employment.Core.Abstraction;
using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.Repositories;
using Employment.Core.Users.Entities;
using Employment.Core.Users.Repositories;

using System;
using System.Threading.Tasks;

namespace Employment.Core.CalendarEvents.UseCases;
public sealed class UpdateCalendarEventUseCase : IUseCase<UpdateCalendarEventUseCase.IInputPort, UpdateCalendarEventUseCase.IOutputPort> {
    private readonly ICalendarEventsRepository eventsRepository;
    private readonly IUsersRepository usersRepository;

    public UpdateCalendarEventUseCase(ICalendarEventsRepository eventsRepository, IUsersRepository usersRepository) {
        this.eventsRepository = eventsRepository;
        this.usersRepository = usersRepository;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        var calendarEvent = await eventsRepository.GetOneAsync(inputPort.EventIdentifier);
        if (calendarEvent == null) {
            outputPort.NotFound($"There is no {nameof(CalendarEvent)} with id '{inputPort.EventIdentifier}'");
            return;
        }

        var user = await usersRepository.GetOneAsync(inputPort.UserIdentifier);
        if (user == null) {
            outputPort.NotFound($"There is no {nameof(User)} with id '{inputPort.UserIdentifier}'");
            return;
        }

        calendarEvent.StartDate = inputPort.StartDate;
        calendarEvent.EndDate = inputPort.EndDate;
        calendarEvent.Status = inputPort.Status;
        calendarEvent.User = user;

        await eventsRepository.UpdateAsync(calendarEvent);

        outputPort.Created(calendarEvent);
    }

    public interface IInputPort : Abstraction.IInputPort {
        long EventIdentifier { get; }
        long UserIdentifier { get; }
        DateOnly StartDate { get; }
        DateOnly EndDate { get; }
        CalendarEventStatus Status { get; }
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Created(CalendarEvent calendarEvent);
        void NotFound(string message);
    }
}
