using Employment.Core.Abstraction;
using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.Repositories;
using Employment.Core.Users.Entities;
using Employment.Core.Users.Repositories;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace Employment.Core.CalendarEvents.UseCases;
public sealed class CreateCalendarEventUseCase : IUseCase<CreateCalendarEventUseCase.IInputPort, CreateCalendarEventUseCase.IOutputPort> {
    private readonly ICalendarEventsRepository eventsRepository;
    private readonly IUsersRepository usersRepository;

    public CreateCalendarEventUseCase(ICalendarEventsRepository eventsRepository, IUsersRepository usersRepository) {
        this.eventsRepository = eventsRepository;
        this.usersRepository = usersRepository;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        if (inputPort.EndDate < inputPort.StartDate) {
            outputPort.NotValid($"End date should be bigger than or equal start date");
            return;
        }

        var user = await usersRepository.GetOneAsync(inputPort.UserIdentifier);
        if (user == null) {
            outputPort.NotFound($"There is no {nameof(User)} with id '{inputPort.UserIdentifier}'");
            return;
        }

        var otherEvent = await eventsRepository.GetManyAsync(e => e.User == user && inputPort.StartDate <= e.EndDate && e.StartDate <= inputPort.EndDate);
        if (otherEvent.Any()) {
            outputPort.Duplicated($"There is already {nameof(CalendarEvent)} in that period");
            return;
        }

        var calendarEvent = new CalendarEvent {
            StartDate = inputPort.StartDate,
            EndDate = inputPort.EndDate,
            Status = inputPort.Status,
            User = user
        };

        await eventsRepository.CreateAsync(calendarEvent);

        outputPort.Created(calendarEvent);
    }

    public interface IInputPort : Abstraction.IInputPort {
        long UserIdentifier { get; }
        DateOnly StartDate { get; }
        DateOnly EndDate { get; }
        CalendarEventStatus Status { get; }
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Created(CalendarEvent calendarEvent);
        void NotFound(string message);
        void NotValid(string message);
        void Duplicated(string message);
    }
}
