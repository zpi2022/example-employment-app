
using Employment.Core.Abstraction;
using Employment.Core.Users.Entities;

namespace Employment.Core.Users.Repositories;
public interface IRolesRepository : IAsyncRepository<long, Role> {
}
