using Employment.Core.Abstraction;
using Employment.Core.Users.Entities;

namespace Employment.Core.Users.Repositories;
public interface IUsersRepository : IAsyncRepository<long, User> {
}
