using Employment.Core.Abstraction;
using Employment.Core.Users.Entities;
using Employment.Core.Users.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Employment.Core.Users.UseCases;
public sealed class GetUsersUseCase : IUseCase<GetUsersUseCase.IOutputPort> {
    private readonly IUsersRepository usersRepository;

    public GetUsersUseCase(IUsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public async Task Execute(NullInputPort inputPort, IOutputPort outputPort) {
        var users = await usersRepository.GetManyAsync();
        outputPort.Found(users);
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Found(IEnumerable<User> users);
    }
}
