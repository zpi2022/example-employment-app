using Employment.Core.Abstraction;
using Employment.Core.Users.Entities;

using Microsoft.AspNetCore.Identity;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Employment.Core.Users.UseCases;
public sealed class UpdateUserUseCase : IUseCase<UpdateUserUseCase.IInputPort, UpdateUserUseCase.IOutputPort> {
    private readonly UserManager<User> userManager;
    private readonly RoleManager<Role> roleManager;

    public UpdateUserUseCase(UserManager<User> userManager, RoleManager<Role> roleManager) {
        this.userManager = userManager;
        this.roleManager = roleManager;
    }

    public async Task Execute(IInputPort inputPort, IOutputPort outputPort) {
        var user = await userManager.FindByIdAsync(inputPort.UserIdentifier.ToString());
        if (user == null) {
            outputPort.NotFound($"There is no {nameof(User)} with id '{inputPort.UserIdentifier}'");
            return;
        }

        user.FirstName = inputPort.FirstName;
        user.LastName = inputPort.LastName;
        user.Email = inputPort.Email;
        user.UserName = inputPort.Email;
        user.PhoneNumber = inputPort.PhoneNumber;

        await userManager.UpdateAsync(user);

        var oldRoles = await userManager.GetRolesAsync(user);
        await userManager.RemoveFromRolesAsync(user, oldRoles);
        await userManager.AddToRolesAsync(user, inputPort.RoleNames);

        outputPort.Updated(user);
    }

    public interface IInputPort : Abstraction.IInputPort {
        long UserIdentifier { get; }
        string FirstName { get; }
        string LastName { get; }
        string Email { get; }
        IEnumerable<string> RoleNames { get; }
        string? PhoneNumber { get; }
    }

    public interface IOutputPort : Abstraction.IOutputPort {
        void Updated(User user);

        void NotFound(string message);
    }
}