using Microsoft.AspNetCore.Identity;

namespace Employment.Core.Users.Entities;
public class UserClaim : IdentityUserClaim<long> {
    public virtual User User { get; set; }
}
