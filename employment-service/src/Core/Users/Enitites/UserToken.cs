using Microsoft.AspNetCore.Identity;

namespace Employment.Core.Users.Entities;
public class UserToken : IdentityUserToken<long> {
    public virtual User User { get; set; }
}
