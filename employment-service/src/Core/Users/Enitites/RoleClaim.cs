using Microsoft.AspNetCore.Identity;

namespace Employment.Core.Users.Entities;
public class RoleClaim : IdentityRoleClaim<long> {
    public virtual Role Role { get; set; }
}
