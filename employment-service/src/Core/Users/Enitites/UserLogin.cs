using Microsoft.AspNetCore.Identity;

namespace Employment.Core.Users.Entities;
public class UserLogin : IdentityUserLogin<long> {
    public virtual User User { get; set; }
}
