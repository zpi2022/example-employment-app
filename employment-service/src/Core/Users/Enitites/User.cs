using Employment.Core.CalendarEvents.Entities;
using Employment.Core.Tasks.Entities;

using Microsoft.AspNetCore.Identity;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Employment.Core.Users.Entities;
public class User : IdentityUser<long> {
    public User() : base() {
    }

    public User(string userName) : base(userName) {
    }

    [PersonalData]
    public string FirstName { get; set; }

    [PersonalData]
    public string LastName { get; set; }

    public virtual ICollection<UserClaim> Claims { get; set; } = new Collection<UserClaim>();
    public virtual ICollection<UserLogin> Logins { get; set; } = new Collection<UserLogin>();
    public virtual ICollection<UserToken> Tokens { get; set; } = new Collection<UserToken>();
    public virtual ICollection<UserRole> UserRoles { get; set; } = new Collection<UserRole>();
    public virtual ICollection<CalendarEvent> CalendarEvents { get; set; } = new Collection<CalendarEvent>();
    public virtual ICollection<TaskEntity> Tasks { get; set; } = new Collection<TaskEntity>();

    public virtual ICollection<Role> Roles { get; set; } = new Collection<Role>();
}
