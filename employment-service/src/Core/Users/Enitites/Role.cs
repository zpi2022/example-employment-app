using Microsoft.AspNetCore.Identity;

using System.Collections.Generic;

namespace Employment.Core.Users.Entities;
public class Role : IdentityRole<long> {
    public Role() : base() {
    }

    public Role(string roleName) : base(roleName) {
    }

    public virtual ICollection<UserRole> UserRoles { get; set; }
    public virtual ICollection<RoleClaim> RoleClaims { get; set; }

    public virtual ICollection<User> Users { get; set; }
}
