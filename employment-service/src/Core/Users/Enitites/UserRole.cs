using Microsoft.AspNetCore.Identity;

namespace Employment.Core.Users.Entities;
public class UserRole : IdentityUserRole<long> {
    public virtual User User { get; set; }
    public virtual Role Role { get; set; }
}
