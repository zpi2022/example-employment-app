using System.Threading.Tasks;

namespace Employment.Core.Abstraction;
public interface IUseCase<in TInputPort, in TOutputPort> where TInputPort : IInputPort
                                                         where TOutputPort : IOutputPort {
    Task Execute(TInputPort inputPort, TOutputPort outputPort);
}

public interface IUseCase<TOutputPort> : IUseCase<NullInputPort, TOutputPort>
    where TOutputPort : IOutputPort {

    Task Execute(TOutputPort outputPort) => Execute(NullInputPort.Instance, outputPort);
}
