using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Employment.Core.Abstraction;

public interface IAsyncRepository<TKey, TEntity> where TEntity : class {
    Task<TEntity?> GetOneAsync(TKey key);
    Task<IEnumerable<TEntity>> GetManyAsync();
    Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate);
    Task<TEntity> CreateAsync(TEntity entity);
    Task<TEntity> DeleteAsync(TEntity entity);
    Task<TEntity> UpdateAsync(TEntity entity);
}
