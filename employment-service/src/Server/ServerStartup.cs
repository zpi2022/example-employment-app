using Employment.Core.Users.Entities;
using Employment.Database;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Employment.Server;

public static class ServerStartup {
    public static IServiceCollection AddServer(this IServiceCollection services) {
        services.AddRazorPages();

        services.AddIdentity<User, Role>()
            .AddIdentityDatabase()
            .AddDefaultTokenProviders()
            .AddDefaultUI();

        services.AddIdentityServer(options => {
            options.Events.RaiseErrorEvents = true;
            options.Events.RaiseInformationEvents = true;
            options.Events.RaiseFailureEvents = true;
            options.Events.RaiseSuccessEvents = true;

            options.EmitStaticAudienceClaim = true;
        })
            .AddDeveloperSigningCredential()
            .AddInMemoryIdentityResources(IdentityConfig.IdentityResources)
            .AddInMemoryApiResources(IdentityConfig.ApiResources)
            .AddInMemoryApiScopes(IdentityConfig.ApiScopes)
            .AddInMemoryClients(IdentityConfig.Clients)
            .AddAspNetIdentity<User>();

        return services;
    }

    public static WebApplication UseServer(this WebApplication app) {
        app.UseIdentityServer();

        app.UseStaticFiles();
        return app;
    }

    public static IEndpointRouteBuilder MapServerEndpoints(this IEndpointRouteBuilder endpoints) {
        endpoints.MapRazorPages();

        return endpoints;
    }
}