using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(Employment.Server.Areas.Identity.IdentityHostingStartup))]
namespace Employment.Server.Areas.Identity;
public class IdentityHostingStartup : IHostingStartup {
    public void Configure(IWebHostBuilder builder) {
        /* Do nothing */
    }
}
