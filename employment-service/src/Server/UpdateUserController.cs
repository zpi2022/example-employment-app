using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Employment.Core.Users;

namespace Employment.Server;

[ApiController]
public class TestController : ControllerBase {
    public TestController(EndpointDataSource endpointsDataSource) {
        this.EndpointsDataSource = endpointsDataSource;
    }

    public EndpointDataSource EndpointsDataSource { get; }

    [HttpGet("test", Name = nameof(ListRazorPages))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(HashSet<string>), StatusCodes.Status200OK)]
    public async Task<IActionResult> ListRazorPages() {
        HashSet<string> Pages = new HashSet<string>();
        foreach (var endpoint in EndpointsDataSource.Endpoints) {
            foreach (var metadata in endpoint.Metadata) {
                if (metadata is PageActionDescriptor) {
                    Pages.Add(((PageActionDescriptor)metadata).RelativePath);
                }
            }
        }

        return Ok(Pages);
    }
}

