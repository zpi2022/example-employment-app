namespace Employment.Server;
using Duende.IdentityServer;
using Duende.IdentityServer.Models;

using IdentityModel;

using System.Collections.Generic;

internal static class IdentityConfig {
    public static class Scopes {
        public const string UsersApi = "users";
        public const string Roles = "roles";
    }

    public static IEnumerable<ApiScope> ApiScopes =>
        new ApiScope[]
        {
            new (Scopes.UsersApi),
        };

    public static IEnumerable<IdentityResource> IdentityResources => new IdentityResource[]
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
            new IdentityResources.Email(),
            new IdentityResource {Name = Scopes.Roles, UserClaims={JwtClaimTypes.Role}}
        };

    public static IEnumerable<ApiResource> ApiResources => new ApiResource[]
        {
            new ApiResource{
                Name = "users-api",
                DisplayName = "Users API",
                Scopes =
                {
                    Scopes.UsersApi
                },
                UserClaims = {
                    JwtClaimTypes.Role
                }
            }

        };

    public static IEnumerable<Client> Clients => new[]
    {
        new Client
        {
            ClientId = "react-spa",
            ClientSecrets = { new Secret("react-spa-secret".Sha256()) },
            ClientName = "Produce SPA React App",
            RequirePkce = true,
            RequireClientSecret = false,
            AllowedGrantTypes = GrantTypes.Code,

            RedirectUris = { "http://localhost:3000/api/auth/callback/react-spa", "http://localhost:3000/signIn/callback"},
            PostLogoutRedirectUris = { "http://localhost:3000/signOut/callback" },
            AllowedCorsOrigins = { "http://localhost:3000" },

            AllowedScopes =
            {
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
                Scopes.UsersApi,
            }
        },
        new Client
        {
            ClientId = "gateway-spa",
            ClientSecrets = { new Secret("gateway-spa".Sha256()) },
            ClientName = "Produce SPA React App Gateway",
            RequirePkce = true,
            RequireClientSecret = true,
            AllowedGrantTypes = GrantTypes.Code,
            AllowOfflineAccess = true,

            RedirectUris = { "https://localhost:4000/signin-oidc"},
            PostLogoutRedirectUris = { "*" },
            AllowedCorsOrigins = { "https://localhost:4000" },

            AlwaysSendClientClaims = true,
            AlwaysIncludeUserClaimsInIdToken = true,


            AllowedScopes =
            {
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
                IdentityServerConstants.StandardScopes.OfflineAccess,
                IdentityServerConstants.StandardScopes.Email,
                Scopes.UsersApi,
                Scopes.Roles,
            }
        }
    };
}