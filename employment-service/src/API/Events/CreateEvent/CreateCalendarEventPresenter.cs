
using Employment.Api.Abstraction;
using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.UseCases;

using Microsoft.AspNetCore.Mvc;

namespace Employment.Api.CalendarEvents;
public class CreateCalendarEventPresenter : ActionResultPresenter, CreateCalendarEventUseCase.IOutputPort {
    public void Created(CalendarEvent calendarEvent) {
        var dto = new CalendarEventDto(calendarEvent);

        ResultSource.SetResult(new OkObjectResult(dto));
    }

    public void NotFound(string message) {
        ResultSource.SetResult(new NotFoundObjectResult(message));
    }

    public void Duplicated(string message) {
        ResultSource.SetResult(new ConflictObjectResult(message));
    }

    public void NotValid(string message) {
        ResultSource.SetResult(new UnprocessableEntityObjectResult(message));
    }
}
