using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Employment.Api.CalendarEvents;

[ApiController]
public class CreateCalendarEventController : ControllerBase {
    public CreateCalendarEventController(CreateCalendarEventUseCase useCase, CreateCalendarEventPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private CreateCalendarEventUseCase UseCase { get; }
    private CreateCalendarEventPresenter Presenter { get; }

    [HttpPost("api/v1/events", Name = nameof(CreateEvent))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<CalendarEventDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> CreateEvent([FromBody] CreateCalendarEventCommand command) {
        await UseCase.Execute(new InputPort(
            command.UserIdentifier,
            DateOnly.FromDateTime(command.StartDate.DateTime),
            DateOnly.FromDateTime(command.EndDate.DateTime),
            command.Status
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(
        long UserIdentifier,
        DateOnly StartDate,
        DateOnly EndDate,
        CalendarEventStatus Status
    ) : CreateCalendarEventUseCase.IInputPort;
}

