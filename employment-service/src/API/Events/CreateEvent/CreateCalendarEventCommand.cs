
using Employment.Core.CalendarEvents.Entities;

using System;
using System.ComponentModel.DataAnnotations;

namespace Employment.Api.CalendarEvents;
public record CreateCalendarEventCommand {
    [Required]
    public long UserIdentifier { get; init; }
    [Required]
    public DateTimeOffset StartDate { get; init; }
    [Required]
    public DateTimeOffset EndDate { get; init; }
    [Required]
    public CalendarEventStatus Status { get; init; }
};

