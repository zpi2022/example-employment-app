using Employment.Api.Abstraction;
using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.UseCases;

using Microsoft.AspNetCore.Mvc;

namespace Employment.Api.CalendarEvents;
public class UpdateCalendarEventPresenter : ActionResultPresenter, UpdateCalendarEventUseCase.IOutputPort {
    public void Created(CalendarEvent calendarEvent) {
        var dto = new CalendarEventDto(calendarEvent);

        ResultSource.SetResult(new OkObjectResult(dto));
    }

    public void NotFound(string message) {
        ResultSource.SetResult(new NotFoundObjectResult(message));
    }

}
