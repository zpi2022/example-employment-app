using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Employment.Api.CalendarEvents;

[ApiController]
public class UpdateCalendarEventController : ControllerBase {
    public UpdateCalendarEventController(UpdateCalendarEventUseCase useCase, UpdateCalendarEventPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private UpdateCalendarEventUseCase UseCase { get; }
    private UpdateCalendarEventPresenter Presenter { get; }

    [HttpPut("api/v1/events/{identifier}", Name = nameof(UpdateEvent))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(CalendarEventDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> UpdateEvent([FromRoute] long identifier, [FromBody] UpdateCalendarEventCommand command) {
        await UseCase.Execute(new InputPort(
            identifier,
            command.UserIdentifier,
            DateOnly.FromDateTime(command.StartDate.DateTime),
            DateOnly.FromDateTime(command.EndDate.DateTime),
            command.Status
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(
        long EventIdentifier,
        long UserIdentifier,
        DateOnly StartDate,
        DateOnly EndDate,
        CalendarEventStatus Status
    ) : UpdateCalendarEventUseCase.IInputPort;
}

