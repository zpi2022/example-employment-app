
using Employment.Core.CalendarEvents.Entities;

using System;
using System.ComponentModel.DataAnnotations;

namespace Employment.Api.CalendarEvents;
public record UpdateCalendarEventCommand {
    [Required]
    public long UserIdentifier { get; init; }
    [Required]
    // public DateOnly StartDate { get; init; }
    public DateTimeOffset StartDate { get; init; }
    [Required]
    public DateTimeOffset EndDate { get; init; }
    // public DateOnly EndDate { get; init; }
    [Required]
    public CalendarEventStatus Status { get; init; }
};

