
using Employment.Core.CalendarEvents.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Employment.Api.CalendarEvents;

[ApiController]
public class GetCalendarEventsController : ControllerBase {
    public GetCalendarEventsController(GetCalendarEventsUseCase useCase, GetCalendarEventsPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private GetCalendarEventsUseCase UseCase { get; }
    private GetCalendarEventsPresenter Presenter { get; }

    [HttpGet("api/v1/events", Name = nameof(GetEvents))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<CalendarEventDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetEvents([FromQuery] long? userIdentifier, [FromQuery] DateOnly? minDate, [FromQuery] DateOnly? maxDate) {
        await UseCase.Execute(new InputPort(
            userIdentifier,
            minDate,
            maxDate
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(
        long? UserIdentifier,
        DateOnly? MinDate,
        DateOnly? MaxDate
    ) : GetCalendarEventsUseCase.IInputPort;
}

