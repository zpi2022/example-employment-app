using Employment.Api.Abstraction;
using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.UseCases;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

namespace Employment.Api.CalendarEvents;
public class GetCalendarEventsPresenter : ActionResultPresenter, GetCalendarEventsUseCase.IOutputPort {
    public void Found(IEnumerable<CalendarEvent> calendarEvent) {
        if (!calendarEvent.Any()) {
            ResultSource.SetResult(new NoContentResult());
            return;
        }

        var dtos = calendarEvent.Select(calendarEvent => new CalendarEventDto(calendarEvent));

        ResultSource.SetResult(new OkObjectResult(dtos));
    }
}
