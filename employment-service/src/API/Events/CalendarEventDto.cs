

using Employment.Core.CalendarEvents.Entities;

using System;
using System.ComponentModel.DataAnnotations;

namespace Employment.Api.CalendarEvents;
public record CalendarEventDto {
    [Required]
    public long Identifier { get; init; }
    [Required]
    public long UserIdentifier { get; init; }
    [Required]
    public DateOnly StartDate { get; init; }
    [Required]
    public DateOnly EndDate { get; init; }
    [Required]
    public CalendarEventStatus Status { get; init; }

    public CalendarEventDto(Core.CalendarEvents.Entities.CalendarEvent calendarEvent) {
        Identifier = calendarEvent.Identifier;
        UserIdentifier = calendarEvent.User.Id;
        StartDate = calendarEvent.StartDate;
        EndDate = calendarEvent.EndDate;
        Status = calendarEvent.Status;
    }
}
