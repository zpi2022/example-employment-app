using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Employment.Api.Tasks;

[ApiController]
public class CreateTaskController : ControllerBase {
    public CreateTaskController(CreateTaskUseCase useCase, CreateTaskPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private CreateTaskUseCase UseCase { get; }
    private CreateTaskPresenter Presenter { get; }

    [HttpPost("api/v1/tasks", Name = nameof(CreateTask))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<TaskDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> CreateTask([FromBody] CreateTaskCommand command) {
        await UseCase.Execute(new InputPort(
            command.Name,
            command.Description,
            command.Priority,
            command.State,
            command.ParentIdentifier,
            command.AssignedUserIdentifier
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(
        string Name,
        string Description,
        int Priority,
        TaskState State,
        long? ParentIdentifier,
        long? AssignedUserIdentifier
    ) : CreateTaskUseCase.IInputPort;
}

