using Employment.Core.Tasks.Entities;

using System.ComponentModel.DataAnnotations;

namespace Employment.Api.Tasks;
public record CreateTaskCommand {
    [Required]
    public string Name { get; init; }
    [Required]
    public string Description { get; init; }
    [Required]
    public int Priority { get; init; }
    [Required]
    public TaskState State { get; init; }
    public long? ParentIdentifier { get; init; }
    public long? AssignedUserIdentifier { get; init; }
};

