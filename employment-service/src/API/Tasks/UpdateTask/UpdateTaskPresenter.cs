using Employment.Api.Abstraction;
using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.UseCases;

using Microsoft.AspNetCore.Mvc;

namespace Employment.Api.Tasks;
public class UpdateTaskPresenter : ActionResultPresenter, UpdateTaskUseCase.IOutputPort {
    public void Created(TaskEntity task) {
        var dto = new TaskDto(task);

        ResultSource.SetResult(new OkObjectResult(dto));
    }

    public void NotFound(string message) {
        ResultSource.SetResult(new NotFoundObjectResult(message));
    }

}
