using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace Employment.Api.Tasks;

[ApiController]
public class UpdateTaskController : ControllerBase {
    public UpdateTaskController(UpdateTaskUseCase useCase, UpdateTaskPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private UpdateTaskUseCase UseCase { get; }
    private UpdateTaskPresenter Presenter { get; }

    [HttpPut("api/v1/tasks/{identifier}", Name = nameof(UpdateTask))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(TaskDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> UpdateTask([FromRoute] long identifier, [FromBody] UpdateTaskCommand command) {
        await UseCase.Execute(new InputPort(
            identifier,
            command.Name,
            command.Description,
            command.Priority,
            command.State,
            command.ParentIdentifier,
            command.AssignedUserIdentifier
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(
        long TaskIdentifier,
        string Name,
        string Description,
        int Priority,
        TaskState State,
        long? ParentIdentifier,
        long? AssignedUserIdentifier
    ) : UpdateTaskUseCase.IInputPort;
}

