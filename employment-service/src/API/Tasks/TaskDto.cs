
using Employment.Core.Tasks.Entities;

using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Employment.Api.Tasks;
public record TaskDto {
    [Required]
    public long Identifier { get; init; }
    [Required]
    public string Name { get; init; }
    [Required]
    public string Description { get; init; }
    [Required]
    public int Priority { get; init; }
    [Required]
    public TaskState State { get; init; }
    [Required]
    public IEnumerable<long> Children { get; init; }
    public long? Parent { get; init; }
    public long? AssignedUserIdentifier { get; init; }

    public TaskDto(Core.Tasks.Entities.TaskEntity task) {
        Identifier = task.Identifier;
        Name = task.Name;
        Description = task.Description;
        Priority = task.Priority;
        State = task.State;
        Children = task.Children.Select(child => child.Identifier);
        Parent = task.Parent?.Identifier;
        AssignedUserIdentifier = task.AssignedUser?.Id;
    }
}
