using Employment.Core.Tasks.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Employment.Api.Tasks;

[ApiController]
public class GetRootTasksController : ControllerBase {
    public GetRootTasksController(GetRootTasksUseCase useCase, GetRootTasksPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private GetRootTasksUseCase UseCase { get; }
    private GetRootTasksPresenter Presenter { get; }

    [HttpGet("api/v1/tasks", Name = nameof(GetRootTasks))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<TaskDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetRootTasks() {
        await UseCase.Execute(new InputPort(
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(

    ) : GetRootTasksUseCase.IInputPort;
}

