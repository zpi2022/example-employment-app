using Employment.Api.Abstraction;
using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.UseCases;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

namespace Employment.Api.Tasks;
public class GetRootTasksPresenter : ActionResultPresenter, GetRootTasksUseCase.IOutputPort {
    public void Found(IEnumerable<TaskEntity> task) {
        if (!task.Any()) {
            ResultSource.SetResult(new NoContentResult());
            return;
        }

        var dtos = task.Select(task => new TaskDto(task)).OrderBy(task => task.Priority); ;

        ResultSource.SetResult(new OkObjectResult(dtos));
    }
}
