using Employment.Core.Tasks.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Employment.Api.Tasks;

[ApiController]
public class GetChildrenTasksController : ControllerBase {
    public GetChildrenTasksController(GetChildrenTasksUseCase useCase, GetChildrenTasksPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private GetChildrenTasksUseCase UseCase { get; }
    private GetChildrenTasksPresenter Presenter { get; }

    [HttpGet("api/v1/tasks/{identifier}/children", Name = nameof(GetChildrenTasks))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<TaskDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetChildrenTasks([FromRoute] long identifier) {
        await UseCase.Execute(new InputPort(
            identifier
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    record InputPort(
        long TaskIdentifier
    ) : GetChildrenTasksUseCase.IInputPort;
}

