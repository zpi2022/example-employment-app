using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace Employment.Api.Abstraction;
public abstract class ActionResultPresenter {
    protected TaskCompletionSource<IActionResult> ResultSource { get; } = new TaskCompletionSource<IActionResult>();
    internal Task<IActionResult> GetResultAsync() => ResultSource.Task;
}
