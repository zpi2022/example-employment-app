using Employment.Api.CalendarEvents;
using Employment.Api.Tasks;
using Employment.Api.Users;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Serilog;

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;

namespace Employment.Api;

public static class ApiStartup {
    public static IServiceCollection AddApi(this IServiceCollection services) {
        services
            .AddControllers(options => options.UseDateOnlyTimeOnlyStringConverters())
            .AddJsonOptions(options =>
                options.UseDateOnlyTimeOnlyStringConverters()
                    .JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter())
            );

        services.AddSwaggerGen(options => {
            options.TagActionsBy(api => new List<string>() { "Employment" });
            options.UseDateOnlyTimeOnlyStringConverters();

            options.UseOneOfForPolymorphism();
            options.UseAllOfForInheritance();

            options.SwaggerDoc("v1", new OpenApiInfo {
                Title = "Employment API",
                Version = "v1"
            });

            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        });

        services.AddScoped<GetUsersPresenter>();
        services.AddScoped<UpdateUserPresenter>();

        services.AddScoped<CreateCalendarEventPresenter>();
        services.AddScoped<GetCalendarEventsPresenter>();
        services.AddScoped<UpdateCalendarEventPresenter>();

        services.AddScoped<CreateTaskPresenter>();
        services.AddScoped<GetRootTasksPresenter>();
        services.AddScoped<GetChildrenTasksPresenter>();
        services.AddScoped<UpdateTaskPresenter>();

        return services;
    }

    public static WebApplication UseApi(this WebApplication app, IWebHostEnvironment env) {
        if (env.IsDevelopment()) {
            app.UseSwagger();
            app.UseSwaggerUI(options => {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Users API v1");
                options.DisplayOperationId();
            });
        }

        app.UseSerilogRequestLogging();

        return app;
    }
    public static IEndpointRouteBuilder MapApiEndpoints(this IEndpointRouteBuilder endpoints) {
        endpoints.MapControllers();

        return endpoints;
    }
}