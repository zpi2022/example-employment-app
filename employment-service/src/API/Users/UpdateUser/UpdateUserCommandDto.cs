
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Employment.Api.Users;
public record UpdateUserCommandDto {
    [Required]
    public string FirstName { get; init; }
    [Required]
    public string LastName { get; init; }
    [Required]
    public string Email { get; init; }
    [Required]
    public IEnumerable<string> Roles { get; init; }
    public string? PhoneNumber { get; init; }
};

