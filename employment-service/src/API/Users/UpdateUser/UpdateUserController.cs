using Employment.Core.Users.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Employment.Api.Users;

[ApiController]
public class UpdateUserController : ControllerBase {
    public UpdateUserController(UpdateUserUseCase useCase, UpdateUserPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private UpdateUserUseCase UseCase { get; }
    private UpdateUserPresenter Presenter { get; }

    [HttpPut("api/v1/users/{identifier}", Name = nameof(UpdateUser))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> UpdateUser([FromRoute] long identifier, [FromBody] UpdateUserCommandDto commandDto) {
        await UseCase.Execute(new InputPort(
            identifier,
            commandDto.FirstName,
            commandDto.LastName,
            commandDto.Email,
            commandDto.PhoneNumber,
            commandDto.Roles
        ), Presenter);

        return await Presenter.GetResultAsync();
    }

    protected record InputPort(
        long UserIdentifier,
        string FirstName,
        string LastName,
        string Email,
        string? PhoneNumber,
        IEnumerable<string> RoleNames
    ) : UpdateUserUseCase.IInputPort;
}

