using Employment.Api.Abstraction;
using Employment.Core.Users.Entities;
using Employment.Core.Users.UseCases;

using Microsoft.AspNetCore.Mvc;

namespace Employment.Api.Users;
public class UpdateUserPresenter : ActionResultPresenter, UpdateUserUseCase.IOutputPort {
    public void NotFound(string message) {
        ResultSource.SetResult(new NotFoundObjectResult(message));
    }

    public void Updated(User user) {
        var dto = new UserDto(user);

        ResultSource.SetResult(new OkObjectResult(dto));
    }
}
