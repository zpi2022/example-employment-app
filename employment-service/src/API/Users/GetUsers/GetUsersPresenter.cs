using Employment.Api.Abstraction;
using Employment.Core.Users.Entities;
using Employment.Core.Users.UseCases;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

namespace Employment.Api.Users;
public class GetUsersPresenter : ActionResultPresenter, GetUsersUseCase.IOutputPort {
    public void Found(IEnumerable<User> users) {
        if (!users.Any()) {
            ResultSource.SetResult(new NoContentResult());
            return;
        }

        var dtos = users.Select(user => new UserDto(user));

        ResultSource.SetResult(new OkObjectResult(dtos));
    }
}
