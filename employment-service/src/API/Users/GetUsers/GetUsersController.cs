using Employment.Core.Abstraction;
using Employment.Core.Users.UseCases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Employment.Api.Users;

[ApiController]
public class GetUsersController : ControllerBase {
    public GetUsersController(GetUsersUseCase useCase, GetUsersPresenter presenter) {
        UseCase = useCase;
        Presenter = presenter;
    }

    private GetUsersUseCase UseCase { get; }
    private GetUsersPresenter Presenter { get; }

    [HttpGet("api/v1/users", Name = nameof(GetUsers))]
    [ProducesDefaultResponseType]
    [ProducesResponseType(typeof(IEnumerable<UserDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetUsers() {
        await UseCase.Execute(NullInputPort.Instance, Presenter);

        return await Presenter.GetResultAsync();
    }
}

