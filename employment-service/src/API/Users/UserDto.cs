
using System.ComponentModel.DataAnnotations;

namespace Employment.Api.Users;
public record UserDto {

    [property: Required]
    public long Identifier { get; init; }
    [property: Required]
    public string FirstName { get; init; }
    [property: Required]
    public string LastName { get; init; }
    [property: Required]
    public string Email { get; init; }
    public string? PhoneNumber { get; init; }

    public UserDto(Core.Users.Entities.User user) {
        Identifier = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        Email = user.Email;
        PhoneNumber = user.PhoneNumber;
    }
}
