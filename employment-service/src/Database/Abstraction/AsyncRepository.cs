using Employment.Core.Abstraction;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Employment.Database.Abstraction;

internal abstract class AsyncRepository<TKey, TEntity> : IAsyncRepository<TKey, TEntity> where TEntity : class {
    private readonly DbContext dbContext;
    public AsyncRepository(DbContext dbContext) {
        this.dbContext = dbContext;
    }

    public async Task<TEntity?> GetOneAsync(TKey key) {
        return await dbContext
            .Set<TEntity>()
            .FindAsync(key);
    }

    public async Task<IEnumerable<TEntity>> GetManyAsync() {
        return await dbContext
            .Set<TEntity>()
            .ToListAsync();
    }

    public async Task<IEnumerable<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate) {
        return await dbContext
            .Set<TEntity>()
            .Where(predicate)
            .ToListAsync();
    }

    public async Task<TEntity> CreateAsync(TEntity entity) {
        var entityEntry = await dbContext
            .Set<TEntity>()
            .AddAsync(entity);
        await dbContext.SaveChangesAsync();
        return entityEntry.Entity;
    }

    public async Task<TEntity> DeleteAsync(TEntity entity) {
        var entityEntry = dbContext
            .Set<TEntity>()
            .Remove(entity);
        await dbContext.SaveChangesAsync();
        return entityEntry.Entity;
    }

    public async Task<TEntity> UpdateAsync(TEntity entity) {
        var entityEntry = dbContext
            .Set<TEntity>()
            .Update(entity);
        await dbContext.SaveChangesAsync();
        return entityEntry.Entity;
    }
}