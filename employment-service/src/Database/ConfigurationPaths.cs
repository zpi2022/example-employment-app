namespace Employment.Database;
internal static class ConfigurationPaths {
    public const string DatabaseSection = "Database";
    public const string UsersDatabaseSection = $"{DatabaseSection}:Users";
}
