using Employment.Core.CalendarEvents.Entities;
using Employment.Core.Tasks.Entities;
using Employment.Core.Users.Entities;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using System.Threading.Tasks;

namespace Employment.Database;
internal sealed class EmploymentDbContext : IdentityDbContext<User, Role, long, UserClaim, UserRole, UserLogin, RoleClaim, UserToken> {
    public const string SCHEMA = "employment";

    public EmploymentDbContext(DbContextOptions<EmploymentDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder) {
        base.OnModelCreating(builder);

        builder.HasDefaultSchema(SCHEMA);

        builder.Entity<User>(b => {
            b.ToTable("Users");

            b.HasMany(e => e.Claims)
                .WithOne(e => e.User)
                .HasForeignKey(uc => uc.UserId)
                .IsRequired();

            b.HasMany(e => e.Logins)
                .WithOne(e => e.User)
                .HasForeignKey(ul => ul.UserId)
                .IsRequired();

            b.HasMany(e => e.Tokens)
                .WithOne(e => e.User)
                .HasForeignKey(ut => ut.UserId)
                .IsRequired();

            b.HasMany(e => e.UserRoles)
                .WithOne(e => e.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();
        });

        builder.Entity<Role>(b => {
            b.ToTable("Roles");

            b.HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();

            b.HasMany(e => e.RoleClaims)
                .WithOne(e => e.Role)
                .HasForeignKey(rc => rc.RoleId)
                .IsRequired();

            b.HasData(new Role[] {
                    new Role { Id = 1L, Name = "Manager", NormalizedName = "MANAGER", ConcurrencyStamp = "3109b6a1-4613-46be-b051-2b3008989902"},
                    new Role { Id = 2L, Name = "Team Leader", NormalizedName = "TEAM LEADER", ConcurrencyStamp = "5b98ebc3-8ea3-4626-ad3b-06ee0bd44daa" },
                    new Role { Id = 3L, Name = "Employee", NormalizedName = "EMPLOYEE", ConcurrencyStamp = "a82201e8-2184-40f4-bdaf-184599768f02" }
                });
        });

        builder.Entity<UserClaim>(b => {
            b.ToTable("UserClaims");
        });

        builder.Entity<UserLogin>(b => {
            b.ToTable("UserLogins");
        });

        builder.Entity<UserToken>(b => {
            b.ToTable("UserTokens");
        });

        builder.Entity<RoleClaim>(b => {
            b.ToTable("RoleClaims");
        });

        builder.Entity<UserRole>(b => {
            b.ToTable("UserRoles");
        });

        builder.Entity<CalendarEvent>(b => {
            b.ToTable("Events");
            b.HasKey(e => e.Identifier);

            b.HasOne(e => e.User)
                .WithMany(e => e.CalendarEvents)
                .HasForeignKey("UserIdentifier")
                .IsRequired();
        });

        builder.Entity<TaskEntity>(b => {
            b.ToTable("Tasks");
            b.HasKey(e => e.Identifier);

            b.HasOne(e => e.AssignedUser)
                .WithMany(e => e.Tasks)
                .HasForeignKey("AssignedUserIdentifier");

            b.HasOne(e => e.Parent)
                .WithMany(e => e.Children)
                .HasForeignKey("ParentIdentifier");
        });
    }
}
