
using Employment.Core.Tasks.Entities;
using Employment.Core.Tasks.Repositories;
using Employment.Database.Abstraction;

namespace Employment.Database.Repositories;
internal class TasksRepository : AsyncRepository<long, TaskEntity>, ITasksRepository {
    public TasksRepository(EmploymentDbContext dbContext) : base(dbContext) {
    }
}

