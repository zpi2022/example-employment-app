using Employment.Core.Users.Entities;
using Employment.Core.Users.Repositories;
using Employment.Database.Abstraction;

namespace Employment.Database.Repositories;
internal class RolesRepository : AsyncRepository<long, Role>, IRolesRepository {
    public RolesRepository(EmploymentDbContext dbContext) : base(dbContext) {
    }
}

