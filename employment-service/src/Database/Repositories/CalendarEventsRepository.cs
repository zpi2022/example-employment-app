using Employment.Core.CalendarEvents.Entities;
using Employment.Core.CalendarEvents.Repositories;
using Employment.Database;
using Employment.Database.Abstraction;

using System;

namespace Calendar.Database.Repositories;

internal class CalendarEventsRepository : AsyncRepository<long, CalendarEvent>, ICalendarEventsRepository {

    public CalendarEventsRepository(EmploymentDbContext dbContext) : base(dbContext) {
    }
}