using Employment.Core.Users.Entities;
using Employment.Core.Users.Repositories;
using Employment.Database.Abstraction;

namespace Employment.Database.Repositories;
internal class UsersRepository : AsyncRepository<long, User>, IUsersRepository {
    public UsersRepository(EmploymentDbContext dbContext) : base(dbContext) {
    }
}

