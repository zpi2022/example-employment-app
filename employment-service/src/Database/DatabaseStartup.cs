using Calendar.Database.Repositories;

using Employment.Core.CalendarEvents.Repositories;
using Employment.Core.Tasks.Repositories;
using Employment.Core.Users.Repositories;
using Employment.Database.Configuration;
using Employment.Database.Repositories;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Employment.Database;

public static class DatabaseStartup {
    public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration) {
        var config = configuration.GetRequiredSection(ConfigurationPaths.UsersDatabaseSection).Get<DatabaseConfiguration>();

        services.AddDbContext<EmploymentDbContext>(contextOptions =>
            contextOptions
                .UseLazyLoadingProxies()
                .UseNpgsql(config.ConnectionString, npgsqlOptions => {
                    npgsqlOptions.MigrationsHistoryTable(config.MigrationsTable, EmploymentDbContext.SCHEMA);
                    npgsqlOptions.SetPostgresVersion(config.DatabaseVersion);
                })
        );


        services.AddScoped<IUsersRepository, UsersRepository>();
        services.AddScoped<IRolesRepository, RolesRepository>();

        services.AddScoped<ICalendarEventsRepository, CalendarEventsRepository>();

        services.AddScoped<ITasksRepository, TasksRepository>();

        return services;
    }

    public static WebApplication UseDatabase(this WebApplication app, IConfiguration configuration) {
        var config = configuration.GetRequiredSection(ConfigurationPaths.UsersDatabaseSection).Get<DatabaseConfiguration>();

        if (config.AutoMigrations) {
            using var scope = app.Services.CreateScope();
            var db = scope.ServiceProvider.GetRequiredService<EmploymentDbContext>();
            db.Database.Migrate();
        }

        return app;
    }

    public static IdentityBuilder AddIdentityDatabase(this IdentityBuilder builder) {
        builder.AddEntityFrameworkStores<EmploymentDbContext>();

        return builder;
    }
}