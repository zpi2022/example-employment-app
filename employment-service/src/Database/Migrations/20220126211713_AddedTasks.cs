﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Employment.Database.Migrations
{
    public partial class AddedTasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tasks",
                schema: "employment",
                columns: table => new
                {
                    Identifier = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    Priority = table.Column<int>(type: "integer", nullable: false),
                    State = table.Column<int>(type: "integer", nullable: false),
                    ParentIdentifier = table.Column<long>(type: "bigint", nullable: true),
                    AssignedUserIdentifier = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Identifier);
                    table.ForeignKey(
                        name: "FK_Tasks_Tasks_ParentIdentifier",
                        column: x => x.ParentIdentifier,
                        principalSchema: "employment",
                        principalTable: "Tasks",
                        principalColumn: "Identifier");
                    table.ForeignKey(
                        name: "FK_Tasks_Users_AssignedUserIdentifier",
                        column: x => x.AssignedUserIdentifier,
                        principalSchema: "employment",
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_AssignedUserIdentifier",
                schema: "employment",
                table: "Tasks",
                column: "AssignedUserIdentifier");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ParentIdentifier",
                schema: "employment",
                table: "Tasks",
                column: "ParentIdentifier");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks",
                schema: "employment");
        }
    }
}
