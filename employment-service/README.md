## Adding migration

```
dotnet ef migrations add <<migration-name>>  --project ./src/Database/Employment.Database.csproj --context EmploymentDbContext --startup-project ./src/Launcher/Employment.Launcher.csproj
```

## Updating Database

```
dotnet ef database update --project ./src/Database/Employment.Database.csproj --context EmploymentDbContext --startup-project ./src/Launcher/Employment.Launcher.csproj
```

## Scaffolding identity pages

```
dotnet aspnet-codegenerator identity --target-framework net6.0 --project ./src/Launcher/Employment.Launcher.csproj --dbContext Employment.Database.EmploymentDbContext --files "<<FILES>>"
```

## Listing scaffoldable pages

```
dotnet aspnet-codegenerator identity --target-framework net6.0 --project ./src/Launcher/Employment.Launcher.csproj --listFiles
```

## List of all scaffoldable pages

```
Account._StatusMessage
Account.AccessDenied
Account.ConfirmEmail
Account.ConfirmEmailChange
Account.ExternalLogin
Account.ForgotPassword
Account.ForgotPasswordConfirmation
Account.Lockout
Account.Login
Account.LoginWith2fa
Account.LoginWithRecoveryCode
Account.Logout
Account.Manage._Layout
Account.Manage._ManageNav
Account.Manage._StatusMessage
Account.Manage.ChangePassword
Account.Manage.DeletePersonalData
Account.Manage.Disable2fa
Account.Manage.DownloadPersonalData
Account.Manage.Email
Account.Manage.EnableAuthenticator
Account.Manage.ExternalLogins
Account.Manage.GenerateRecoveryCodes
Account.Manage.Index
Account.Manage.PersonalData
Account.Manage.ResetAuthenticator
Account.Manage.SetPassword
Account.Manage.ShowRecoveryCodes
Account.Manage.TwoFactorAuthentication
Account.Register
Account.RegisterConfirmation
Account.ResendEmailConfirmation
Account.ResetPassword
Account.ResetPasswordConfirmation
```
