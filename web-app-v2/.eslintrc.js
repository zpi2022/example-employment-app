module.exports = {
  extends: ['mantine', 'plugin:@next/next/recommended'],
  parserOptions: {
    project: './tsconfig.json',
  },

  rules: {
    'react/react-in-jsx-scope': 'off',
    '@typescript-eslint/space-before-blocks': 'off',
    'object-shorthand': 'off',
  },
};
