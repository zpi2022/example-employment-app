import { ColorScheme, MantineColor, MantineThemeOverride } from '@mantine/core';

export function createTheme(
  colorMode: ColorScheme,
  primaryColor: MantineColor,
  locale: string
): MantineThemeOverride {
  return {
    colorScheme: colorMode,
    primaryColor: primaryColor,
    datesLocale: locale,
  };
}
