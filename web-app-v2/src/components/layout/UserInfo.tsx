import { Avatar, Box, Group, Text, UnstyledButton, UnstyledButtonProps } from '@mantine/core';
import React, { ForwardedRef, forwardRef } from 'react';
import { MdChevronRight } from 'react-icons/md';

function UserButtonFC(props: UnstyledButtonProps, ref: ForwardedRef<HTMLButtonElement>) {
  return (
    <UnstyledButton
      ref={ref}
      {...props}
      sx={(theme) => ({
        display: 'block',
        width: '100%',
        padding: theme.spacing.xs,
        color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

        '&:hover': {
          backgroundColor:
            theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
        },
      })}
    >
      <Group direction="row" noWrap>
        <Avatar radius="xl" color="grape">
          FN
        </Avatar>
        <Box sx={{ flex: 1, overflow: 'hidden' }}>
          <Text size="sm" weight={500} sx={{ textOverflow: 'ellipsis', overflow: 'hidden' }}>
            Full Name
          </Text>
          <Text color="dimmed" size="xs" sx={{ textOverflow: 'ellipsis', overflow: 'hidden' }}>
            email@email
          </Text>
        </Box>
        <MdChevronRight />
      </Group>
    </UnstyledButton>
  );
}

export const UserButton = forwardRef(UserButtonFC);
