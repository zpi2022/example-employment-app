import {
  ColorSwatch,
  Divider,
  Grid,
  MANTINE_COLORS,
  Menu,
  MenuProps,
  useMantineColorScheme,
  useMantineTheme,
} from '@mantine/core';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { MdCheck, MdDarkMode, MdLightMode, MdLogout } from 'react-icons/md';
import { useClaims } from '../../hooks/useClaims';
import { useUserPreferences } from '../../hooks/useUserPreferences';

interface UserMenuProps {
  control?: MenuProps['control'];
}

export function UserMenu({ control }: UserMenuProps) {
  const router = useRouter();
  const theme = useMantineTheme();
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  const { primaryColor, setPrimaryColor } = useUserPreferences();
  const claims = useClaims();

  function logout() {
    router.push(`/bff/logout?sid=${claims?.sid}`);
  }
  useEffect(() => {
    console.log('USER MENU', primaryColor);
  }, [primaryColor]);

  return (
    <Menu
      withArrow
      placement="end"
      position="right"
      control={control}
      styles={{
        root: {
          width: '100%',
        },
        body: {
          marginBottom: 10,
        },
      }}
    >
      <Menu.Item
        icon={colorScheme === 'dark' ? <MdDarkMode /> : <MdLightMode />}
        onClick={() => toggleColorScheme()}
      >
        Toggle color scheme
      </Menu.Item>
      <Menu.Label>
        <Grid gutter="xs" columns={MANTINE_COLORS.length / 2}>
          {MANTINE_COLORS.map((color) => (
            <Grid.Col span={1} key={color}>
              <ColorSwatch
                size={18}
                radius="sm"
                component="button"
                color={theme.colors[color][8]}
                onClick={() => setPrimaryColor(color)}
                sx={{ cursor: 'pointer' }}
              >
                {primaryColor === color && <MdCheck color="white" />}
              </ColorSwatch>
            </Grid.Col>
          ))}
        </Grid>
      </Menu.Label>
      <Divider />
      <Menu.Item icon={<MdLogout />} onClick={logout}>
        Logout
      </Menu.Item>
    </Menu>
  );
}
