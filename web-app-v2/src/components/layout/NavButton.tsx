import {
  Group,
  MantineColor,
  Text,
  ThemeIcon,
  Tooltip,
  Transition,
  UnstyledButton,
} from '@mantine/core';
import Link from 'next/link';
import { ReactNode } from 'react';

interface NavButtonProps {
  icon: ReactNode;
  color: MantineColor;
  label: string;
  href: string;
  selected?: boolean;
  collapsed?: boolean;
}

export function NavButton({ icon, color, label, href, selected, collapsed }: NavButtonProps) {
  return (
    <Link href={href} passHref>
      <UnstyledButton
        sx={(theme) => ({
          display: 'block',
          width: '100%',
          padding: theme.spacing.xs,
          borderRadius: theme.radius.sm,
          color: theme.colorScheme === 'dark' ? theme.white : theme.black,

          ...(selected && {
            backgroundColor: theme.fn.rgba(theme.colors[theme.primaryColor][6], 0.25),
          }),
          ...(!selected && {
            '&:hover': {
              backgroundColor:
                theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
            },
          }),
          ...(!selected && {
            '&:hover': {
              backgroundColor:
                theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
            },
          }),
        })}
      >
        <Group noWrap>
          {collapsed && (
            <Tooltip label={label} position="right">
              <ThemeIcon color={color} variant="light">
                {icon}
              </ThemeIcon>
            </Tooltip>
          )}

          {!collapsed && (
            <ThemeIcon color={color} variant="light">
              {icon}
            </ThemeIcon>
          )}

          <Transition mounted={!collapsed} transition="slide-left">
            {(styles) => (
              <Text
                size="sm"
                sx={{
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
                  whiteSpace: 'nowrap',
                  ...styles,
                }}
              >
                {label}
              </Text>
            )}
          </Transition>
        </Group>
      </UnstyledButton>
    </Link>
  );
}
