import { MantineColor } from '@mantine/core';
import { ReactNode } from 'react';

export interface NavigationItem {
  name: string;
  href: string;
  icon: ReactNode;
  color: MantineColor;
}
