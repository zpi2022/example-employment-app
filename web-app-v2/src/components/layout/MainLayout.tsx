import { AppShell, useMantineTheme } from '@mantine/core';
import { AnimatePresence } from 'framer-motion';
import { useRouter } from 'next/router';
import { ReactNode, useEffect } from 'react';
import { MotionBox } from '../common/MotionBox';
import { NavigationItem } from './NavigationItem';
import { Sidebar } from './Sidebar';

export interface MainLayoutProps {
  navItems: NavigationItem[];
  children: ReactNode;
}

export function MainLayout({ navItems, children }: MainLayoutProps) {
  const theme = useMantineTheme();
  const router = useRouter();

  useEffect(() => console.log('THEME', theme), []);

  return (
    <AppShell
      styles={{
        main: {
          overflow: 'hidden',
        },
        body: {
          height: '100vh',
          width: '100vw',
        },
      }}
      navbar={<Sidebar items={navItems} />}
      padding="xs"
    >
      <AnimatePresence exitBeforeEnter>
        <MotionBox
          key={router.asPath}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.3 }}
          sx={{
            width: '100%',
            height: '100%',
          }}
        >
          {children}
        </MotionBox>
      </AnimatePresence>
    </AppShell>
  );
}
