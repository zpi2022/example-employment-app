import {
  Avatar,
  Box,
  Group,
  Text,
  Transition,
  UnstyledButton,
  UnstyledButtonProps,
} from '@mantine/core';
import { ForwardedRef, forwardRef } from 'react';
import { MdChevronRight } from 'react-icons/md';
import { useClaims } from '../../hooks/useClaims';

interface UserButtonProps extends UnstyledButtonProps<'button'> {
  collapsed?: boolean;
}

function UserButtonFC(
  { collapsed, ...props }: UserButtonProps,
  ref: ForwardedRef<HTMLButtonElement>
) {
  const claims = useClaims();

  return (
    <UnstyledButton
      ref={ref}
      {...props}
      sx={(theme) => ({
        display: 'block',
        width: '100%',
        padding: theme.spacing.xs,
        color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

        '&:hover': {
          backgroundColor:
            theme.colorScheme === 'dark' ? theme.colors.dark[6] : theme.colors.gray[0],
        },
      })}
    >
      <Group direction="row" noWrap spacing={0}>
        <Avatar size={42} radius="xl" color="grape" src={claims?.picture} />
        <Transition mounted={!collapsed} transition="slide-left">
          {(styles) => (
            <>
              <Box
                ml="xs"
                sx={{
                  flex: 1,
                  overflow: 'hidden',
                  ...styles,
                }}
              >
                <Text
                  size="sm"
                  weight={500}
                  sx={{
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                  }}
                >
                  {claims?.name ?? ''}
                </Text>
                <Text
                  color="dimmed"
                  size="xs"
                  sx={{
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                  }}
                >
                  {claims?.email ?? ''}
                </Text>
              </Box>
              <MdChevronRight style={styles} />
            </>
          )}
        </Transition>
      </Group>
    </UnstyledButton>
  );
}

export const UserButton = forwardRef(UserButtonFC);
