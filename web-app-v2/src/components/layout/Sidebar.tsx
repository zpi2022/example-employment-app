import {
  ActionIcon,
  Divider,
  Group,
  MantineColor,
  Navbar,
  NavbarProps,
  ScrollArea,
} from '@mantine/core';
import { useBooleanToggle } from '@mantine/hooks';
import { useRouter } from 'next/router';
import { ReactNode } from 'react';
import { HiChevronDoubleLeft, HiChevronDoubleRight } from 'react-icons/hi';
import { Logo } from '../common/Logo';
import { NavButton } from './NavButton';
import { UserButton } from './UserButton';
import { UserMenu } from './UserMenu';

export interface NavigationItem {
  name: string;
  href: string;
  icon: ReactNode;
  color: MantineColor;
}

export interface SidebarProps extends Omit<NavbarProps, 'children'> {
  items: NavigationItem[];
}

export function Sidebar({ items, ...props }: SidebarProps) {
  const router = useRouter();
  const [isCollapsed, toggleCollapsed] = useBooleanToggle();

  return (
    <Navbar
      {...props}
      width={{ base: isCollapsed ? 70 : 240 }}
      sx={(theme) => ({
        transitionProperty: 'width',
        transitionTimingFunction: theme.transitionTimingFunction,
        transitionDuration: '300ms',
        overflowX: 'hidden',
      })}
    >
      <Navbar.Section mt="xs" mx="xs">
        <Group
          spacing="xs"
          align="center"
          position="apart"
          direction={isCollapsed ? 'column' : 'row'}
          noWrap
        >
          <Logo withName={!isCollapsed} />
          <ActionIcon onClick={() => toggleCollapsed()}>
            {isCollapsed && <HiChevronDoubleRight />}
            {!isCollapsed && <HiChevronDoubleLeft />}
          </ActionIcon>
        </Group>
      </Navbar.Section>
      <Divider mt="xs" mx="xs" />
      <Navbar.Section
        grow
        component={ScrollArea}
        p="sm"
        styles={{
          viewport: {
            overflowX: 'hidden',
          },
        }}
      >
        {/* <Box m="sm"> */}
        {items.map((nav) => (
          <NavButton
            icon={nav.icon}
            color={nav.color}
            label={nav.name}
            href={nav.href}
            selected={router.asPath.includes(nav.href)}
            collapsed={isCollapsed}
            key={nav.href}
          />
        ))}
        {/* </Box> */}
      </Navbar.Section>
      <Divider mx="xs" />
      <Navbar.Section>
        <UserMenu control={<UserButton collapsed={isCollapsed} />} />
      </Navbar.Section>
    </Navbar>
  );
}
