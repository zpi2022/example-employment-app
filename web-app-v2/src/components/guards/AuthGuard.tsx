import { useRouter } from 'next/router';
import React, { ReactNode, useEffect } from 'react';
import { useClaims } from '../../hooks/useClaims';
import Loader from '../common/Loader';

interface AuthGuardProps {
  children?: ReactNode;
}

export const AuthGuard = ({ children }: AuthGuardProps) => {
  const user = useClaims();
  const router = useRouter();

  useEffect(() => {
    if (user === null) {
      router.replace('/bff/login?returnUrl=/');
    }
  }, [user]);

  if (user === undefined) {
    return <Loader />;
  }

  return <>{children}</>;
};
