import { Center, Loader as MantineLoader, Text } from '@mantine/core';
import React from 'react';

interface LoaderProps {
  message?: string;
}

const Loader = ({ message }: LoaderProps) => (
  <Center
    sx={{
      width: '100%',
      height: '100%',
    }}
  >
    <MantineLoader size="xl" variant="bars" />
    {message && <Text mt={2}>{message}</Text>}
  </Center>
);

export default Loader;
