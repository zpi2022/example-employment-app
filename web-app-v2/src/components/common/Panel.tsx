import { Paper, Text } from '@mantine/core';
import React from 'react';

interface PanelProps {
  title?: string;
  contentPadding?: boolean;
  flex?: number | string;
  allowScrolling?: boolean;
  children?: React.ReactNode;
}

const Panel = ({ title, contentPadding, flex, allowScrolling, children }: PanelProps) => (
  <Paper
    shadow="md"
    withBorder
    p={contentPadding ? 'md' : undefined}
    sx={{
      padding: 0,
      position: 'relative',
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
      flex: flex,
    }}
  >
    {title && <Text>{title}</Text>}
    {children}
  </Paper>
);

export default Panel;
