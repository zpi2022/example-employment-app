import { Box, BoxProps } from '@mantine/core';
import { motion } from 'framer-motion';

export const MotionBox = motion<BoxProps<'div'>>(Box);
