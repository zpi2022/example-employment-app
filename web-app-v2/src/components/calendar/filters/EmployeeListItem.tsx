// import { Avatar, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText } from '@mui/material';
// import { UserDto } from 'employment-api';
// import React, { useMemo } from 'react';

// interface FiltersItemProps {
//   employee: UserDto;
// }

// function EmployeeListItemComponent({ employee }: FiltersItemProps) {
//   const initials = useMemo(() => `${employee.firstName[0] ?? ''}${employee.lastName[0] ?? ''}`, [employee.firstName, employee.lastName]);

//   function handleChange() {
//     if (!appState.removeFromSelected(props.employee.id)) {
//       appState.addToSelected(props.employee.id);
//     }
//   }

//   return (
//     <ListItem button onClick={handleChange}>
//       <ListItemAvatar>
//         <Avatar>{initials}</Avatar>
//       </ListItemAvatar>
//       <ListItemText primary={`${employee.firstName} ${employee.lastName}`} />
//       <ListItemSecondaryAction>{/* <Checkbox edge="end" checked={appState.selectedEmployeeIds.includes(props.employee.id)} onChange={handleChange} /> */}</ListItemSecondaryAction>
//     </ListItem>
//   );
// }

// export const EmployeeListItem = EmployeeListItemComponent;
