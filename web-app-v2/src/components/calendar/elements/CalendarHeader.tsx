import { ActionIcon, Button, Group, SegmentedControl, Stack, Text, Tooltip } from '@mantine/core';
import { useModals } from '@mantine/modals';
import React from 'react';
import { MdNavigateBefore, MdNavigateNext, MdToday } from 'react-icons/md';
import { getMonthName } from '../../../utils/DateUtils';
import { CalendarType } from '../CalendarType';
import { NewLeaveDialog } from '../leaves/NewLeaveDialog';

interface CalendarHeaderProps {
  activeType: CalendarType;
  activeDate: Date;

  onNext(): void;
  onBefore(): void;
  onToday(): void;
  onTypeChange(type: CalendarType): void;
}

export function CalendarHeader({
  activeType,
  activeDate,
  onBefore,
  onNext,
  onToday,
  onTypeChange,
}: CalendarHeaderProps) {
  const modals = useModals();

  function handleTypeChange(newValue: string): void {
    if (newValue !== null) {
      onTypeChange(newValue as CalendarType);
    }
  }

  const openNewEventModal = () => {
    const id = modals.openModal({
      title: 'Create event',
      centered: true,
      children: <NewLeaveDialog onClose={() => modals.closeModal(id)} />,
      withCloseButton: false,
    });
  };

  return (
    <Group position="apart" m="md" grow>
      <Group position="left">
        <ActionIcon onClick={onBefore} size="lg">
          <MdNavigateBefore size={32} />
        </ActionIcon>

        <Tooltip label="Today">
          <ActionIcon onClick={onToday} size="lg">
            <MdToday size={24} />
          </ActionIcon>
        </Tooltip>
        <Button compact onClick={openNewEventModal}>
          Add new
        </Button>
      </Group>
      <Stack spacing={0}>
        <Text align="center">{getMonthName(activeDate, 'long')}</Text>
        <Text align="center" color="textSecondary">
          {activeDate.getFullYear()}
        </Text>
      </Stack>
      <Group position="right">
        <SegmentedControl
          data={Object.keys(CalendarType).map((value) => ({ label: value, value: value }))}
          value={activeType}
          onChange={handleTypeChange}
        />
        <ActionIcon onClick={onNext} size="lg">
          <MdNavigateNext size={32} />
        </ActionIcon>
      </Group>
    </Group>
  );
}
