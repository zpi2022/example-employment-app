import { Box, Text } from '@mantine/core';
import dayjs from 'dayjs';
import React, { useMemo } from 'react';
import { isSameFullDate, isSameMonth } from '../../../utils/DateUtils';
import { LeaveEventsWrapper } from '../leaves/LeavesWrapper';

interface CalendarRowProps {
  columns: number;
  startDate: Date;
  activeDate: Date;
  markedDate?: Date;
  loadEvents?: boolean;
  borders?: boolean;
  onShowWeek?: (activeDate: Date) => void;
}

export function WeekRow({
  columns,
  startDate,
  activeDate,
  markedDate,
  loadEvents,
  borders,
  onShowWeek,
}: CalendarRowProps) {
  console.log('Week row start', startDate.toISOString());

  const cellDates = useMemo(() => {
    const dates: Array<Date> = [];
    const startDateDayJs = dayjs(startDate);

    for (let i = 0; i < columns; i += 1) {
      dates.push(startDateDayJs.add(i, 'days').toDate());
    }

    return dates;
  }, []);

  return (
    <Box
      sx={(theme) => ({
        ...(borders && {
          borderBottom: `${
            theme.colorScheme === 'dark' ? theme.colors.dark[5] : theme.colors.gray[3]
          } thin solid`,
          '&:last-child': {
            border: 'none',
          },
        }),
        flex: 1,
        position: 'relative',
        minHeight: '2em',
        paddingTop: '2em',
        overflow: 'hidden',
      })}
    >
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: `repeat(${columns}, 1fr)`,
          gridTemplateRows: '1fr',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          position: 'absolute',
        }}
      >
        {cellDates.map((date) => (
          <Box
            key={date.toISOString().substring(0, 10)}
            sx={(theme) => ({
              ...(borders && {
                borderRight: `${
                  theme.colorScheme === 'dark' ? theme.colors.dark[5] : theme.colors.gray[3]
                } thin solid`,
                '&:last-child': {
                  border: 'none',
                },
              }),
              textAlign: 'center',
            })}
          >
            <Text
              mx={4}
              my={4}
              size="sm"
              weight={500}
              color={!isSameMonth(activeDate, date) ? 'dimmed' : undefined}
              sx={(theme) => ({
                borderRadius: theme.radius.xl,
                ...(isSameFullDate(markedDate, date) && {
                  background:
                    theme.colorScheme === 'dark' ? theme.colors.gray[8] : theme.colors.gray[3],
                }),
                ...(isSameFullDate(activeDate, date) && {
                  color: theme.colorScheme === 'dark' ? theme.white : theme.black,
                  background:
                    theme.colorScheme === 'dark'
                      ? theme.colors[theme.primaryColor][6]
                      : theme.colors[theme.primaryColor][4],
                }),
              })}
            >
              {date.getDate()}
            </Text>
          </Box>
        ))}
      </Box>
      {loadEvents && (
        <LeaveEventsWrapper columns={columns} startDate={startDate} onShowWeek={onShowWeek} />
      )}
    </Box>
  );
}
