import { Stack } from '@mantine/core';
import dayjs from 'dayjs';
import React, { useMemo } from 'react';
import { WeekRow } from './WeekRow';

interface CalendarGridProps {
  columns: number;
  rows: number;
  activeDate: Date;
  startDate: Date;
  markedDate?: Date;
  loadEvents?: boolean;
  borders?: boolean;
  onShowWeek?: (activeDate: Date) => void;
}

export function WeeksGrid({
  columns,
  rows,
  activeDate,
  startDate,
  markedDate,
  loadEvents,
  borders,
  onShowWeek,
}: CalendarGridProps) {
  const weekStartDates = useMemo(() => {
    const dates: Array<Date> = [];
    const startDateTemp = dayjs(startDate);

    for (let i = 0; i < rows; i += 1) {
      dates.push(startDateTemp.add(i, 'weeks').toDate());
    }

    return dates;
  }, [startDate, rows]);

  return (
    <Stack spacing={0} sx={{ flex: 1 }}>
      {weekStartDates.map((date) => (
        <WeekRow
          columns={columns}
          startDate={date}
          activeDate={activeDate}
          markedDate={markedDate}
          loadEvents={loadEvents}
          borders={borders}
          onShowWeek={onShowWeek}
          key={date.toISOString().substring(0, 10)}
        />
      ))}
    </Stack>
  );
}
