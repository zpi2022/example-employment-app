import { Box, Text } from '@mantine/core';
import dayjs from 'dayjs';
import React from 'react';
import { getWeekdayName, WeekdayNameVariant } from '../../../utils/DateUtils';

interface CalendarHeaderProps {
  columns: number;
  firstColumnDate: Date;
  variant?: WeekdayNameVariant;
}

export function WeekdayNames(props: CalendarHeaderProps) {
  function createHeaders() {
    const headers: Array<JSX.Element> = [];
    const startDate = dayjs(props.firstColumnDate);

    for (let i = 0; i < props.columns; i += 1) {
      const columnDate = startDate.add(i, 'days');
      headers.push(
        <Text key={columnDate.day()}>
          {getWeekdayName(columnDate.toDate(), props.variant || 'short')}
        </Text>
      );
    }

    return headers;
  }

  return (
    <Box
      sx={{
        display: 'grid',
        width: '100%',
        gridTemplateRows: '1fr',
        gridAutoFlow: 'column',
        gridAutoColumns: '1fr',
        justifyItems: 'center',
        textTransform: 'uppercase',
      }}
    >
      {createHeaders()}
    </Box>
  );
}
