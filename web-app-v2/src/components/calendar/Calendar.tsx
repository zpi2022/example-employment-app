import { Stack } from '@mantine/core';
import dayjs from 'dayjs';
import React, { ReactNode, useMemo } from 'react';
import { WeekdayNameVariant } from '../../utils/DateUtils';
import { CalendarType } from './CalendarType';
import { WeekdayNames } from './elements/WeekdayNames';
import { WeeksGrid } from './elements/WeeksGrid';

interface CalendarProps {
  type: CalendarType;
  activeDate: Date;
  markedDate: Date;
  weekdayVariant?: WeekdayNameVariant;
  header?: ReactNode;
  borders?: boolean;
  loadEvents?: boolean;
  onShowWeek?: (activeDate: Date) => void;
}

export function Calendar({
  type,
  activeDate,
  markedDate,
  weekdayVariant,
  header,
  borders,
  loadEvents,
  onShowWeek,
}: CalendarProps) {
  function calculateStartDate(): Date {
    if (type === CalendarType.MONTH) {
      return dayjs(activeDate).startOf('month').startOf('week').toDate();
    }
    return dayjs(activeDate).startOf('week').toDate();
  }

  // eslint-disable-next-line consistent-return
  function getRowsNumber(): number {
    switch (type) {
      case CalendarType.MONTH:
        return 6;
      case CalendarType.WEEK:
        return 1;
    }
  }

  const startDate = useMemo(calculateStartDate, [activeDate, type]);
  const rowsNumber = useMemo(getRowsNumber, [type]);
  return (
    <Stack spacing={0} sx={{ height: '100%' }}>
      {header}
      <WeekdayNames columns={7} firstColumnDate={startDate} variant={weekdayVariant} />
      <WeeksGrid
        columns={7}
        rows={rowsNumber}
        activeDate={activeDate}
        markedDate={markedDate}
        startDate={startDate}
        loadEvents={loadEvents}
        borders={borders}
        onShowWeek={onShowWeek}
      />
    </Stack>
  );
}
