import { Button, Chip, Chips, Group, Stack } from '@mantine/core';
import { showNotification } from '@mantine/notifications';
import dayjs from 'dayjs';
import React, { useMemo, useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';
import { useClaims } from '../../../hooks/useClaims';
import { CalendarEventDto, CalendarEventStatus, UpdateCalendarEventCommand } from '../../../models';
import { fetchFromApi, HTTPMethod } from '../../../utils/FetchData';
import { Leave } from './Leave';

interface UpdateLeaveDialogProps {
  leave: Leave;
  onClose: () => void;
}

export function UpdateLeaveDialog({ leave, onClose }: UpdateLeaveDialogProps) {
  const [selectedStatus, setSelectedStatus] = useState(leave.status);
  const claims = useClaims();
  const queryClient = useQueryClient();

  const updateEventMutation = useMutation(
    (command: UpdateCalendarEventCommand & { identifier: number }) =>
      fetchFromApi<CalendarEventDto>({
        path: `/api/v1/events/${command.identifier}`,
        method: HTTPMethod.PUT,
        body: command,
      }),
    {
      onSuccess: () => {
        queryClient.invalidateQueries([
          'events',
          dayjs(leave.startDate).weekday(0).toISOString().substring(0, 10),
          dayjs(leave.endDate).weekday(6).toISOString().substring(0, 10),
        ]);
      },
      onError: () => {
        showNotification({ message: 'Error occured while updating event', color: 'red' });
      },
    }
  );

  function handleDialogClose() {
    onClose();
    setSelectedStatus(leave.status);
  }

  function handleDialogSave() {
    updateEventMutation.mutate({
      endDate: leave.endDate,
      startDate: leave.startDate,
      status: selectedStatus,
      userIdentifier: leave.userIdentifier,
      identifier: leave.identifier,
    });
    onClose();
  }

  const availableStatuses = useMemo(() => {
    if (claims?.role.includes('Manager')) {
      return Object.values(CalendarEventStatus);
    }
    return [CalendarEventStatus.PENDING, CalendarEventStatus.CANCELLED];
  }, [claims?.role]);

  return (
    <Stack>
      <Chips
        value={selectedStatus}
        onChange={(value: CalendarEventStatus) => setSelectedStatus(value)}
      >
        {availableStatuses.map((status) => (
          <Chip value={status} key={status}>
            {status}
          </Chip>
        ))}
      </Chips>
      <Group position="right">
        <Button variant="outline" onClick={handleDialogClose}>
          Cancel
        </Button>
        <Button onClick={handleDialogSave}>Confirm</Button>
      </Group>
    </Stack>
  );
}
