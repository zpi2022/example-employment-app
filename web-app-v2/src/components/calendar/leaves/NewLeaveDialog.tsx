import { Button, Group, Stack } from '@mantine/core';
import { DatePicker } from '@mantine/dates';
import { showNotification } from '@mantine/notifications';
import React, { useMemo, useState } from 'react';
import { useMutation } from 'react-query';
import { useClaims } from '../../../hooks/useClaims';
import { CalendarEventDto, CalendarEventStatus, CreateCalendarEventCommand } from '../../../models';
import { getToday } from '../../../utils/DateUtils';
import { fetchFromApi, HTTPMethod } from '../../../utils/FetchData';

interface NewLeaveDialogProps {
  onClose: () => void;
}

export function NewLeaveDialog({ onClose }: NewLeaveDialogProps) {
  const today = useMemo(getToday, []);
  const [startDate, setStartDate] = useState(today);
  const [endDate, setEndDate] = useState(today);
  const claims = useClaims();

  const newEventMutation = useMutation(
    (command: CreateCalendarEventCommand) =>
      fetchFromApi<CalendarEventDto>({
        path: '/api/v1/events',
        method: HTTPMethod.POST,
        body: command,
      }),
    {
      onError: () => {
        showNotification({ message: 'Error occured while updating event', color: 'red' });
      },
    }
  );

  function handleDialogClose() {
    onClose();
  }

  function handleDialogSave() {
    newEventMutation.mutate({
      endDate: endDate,
      startDate: startDate,
      status: CalendarEventStatus.PENDING,
      userIdentifier: parseInt(claims!.sub, 10),
    });
    onClose();
  }

  function handleStartDateChange(newStartDate: Date) {
    setStartDate(newStartDate);
    if (endDate < newStartDate) {
      setEndDate(newStartDate);
    }
  }

  function handleEndDateChange(newEndDate: Date) {
    setEndDate(newEndDate);
    if (startDate > newEndDate) {
      setStartDate(newEndDate);
    }
  }

  return (
    <Stack>
      <Group noWrap>
        <DatePicker
          label="Leave start date"
          value={startDate}
          onChange={handleStartDateChange}
          minDate={today}
        />
        <DatePicker
          label="Leave end date"
          value={endDate}
          onChange={handleEndDateChange}
          minDate={today}
        />
      </Group>
      <Group position="right">
        <Button variant="outline" onClick={handleDialogClose}>
          Cancel
        </Button>
        <Button onClick={handleDialogSave}>Confirm</Button>
      </Group>
    </Stack>
  );
}
