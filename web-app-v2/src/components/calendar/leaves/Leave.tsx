import { CalendarEventStatus } from '../../../models';

export interface Leave {
  identifier: number;
  name: string;
  startDate: Date;
  endDate: Date;
  status: CalendarEventStatus;
  userIdentifier: number;
}
