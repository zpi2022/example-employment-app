import { Box, Text } from '@mantine/core';
import React, { useLayoutEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { useResizeDetector } from 'react-resize-detector';
import { useClaims } from '../../../hooks/useClaims';
import { CalendarEventDto, UserDto } from '../../../models';
import { addToDate, asUTC } from '../../../utils/DateUtils';
import { createApiFetcher } from '../../../utils/FetchData';
import { LeaveEvent } from './LeaveEvent';

interface LeavesWrapperProps {
  columns: number;
  startDate: Date;
  onShowWeek?: (activeDate: Date) => void;
}

export function LeaveEventsWrapper({ columns, startDate, onShowWeek }: LeavesWrapperProps) {
  const [isOverflow, setIsOverflow] = useState(false);
  const { width, height, ref: targetRef } = useResizeDetector();
  const claims = useClaims();
  const endDate = addToDate(startDate, columns - 1, 'days');

  const { data: users } = useQuery('users', createApiFetcher<UserDto[]>({ path: '/api/v1/users' }));
  const { data: events } = useQuery(
    [
      'events',
      asUTC(startDate).toISOString().substring(0, 10),
      asUTC(endDate).toISOString().substring(0, 10),
    ],
    createApiFetcher<CalendarEventDto[]>({
      path: '/api/v1/events',
      query: {
        userIdentifier: claims?.role.includes('Manager') ? null : claims!.sub,
        minDate: asUTC(startDate).toISOString().substring(0, 10),
        maxDate: asUTC(endDate).toISOString().substring(0, 10),
      },
    })
  );

  useLayoutEffect(() => {
    if (targetRef?.current) {
      setIsOverflow(targetRef.current.scrollHeight > targetRef.current.clientHeight);
    }
  }, [targetRef, targetRef.current, height, width, events?.length]);

  return (
    <Box ref={targetRef} sx={{ height: '100%', overflow: 'hidden' }}>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: `repeat(${columns}, 1fr)`,
          gridAutoFlow: 'dense',
          alignContent: 'start',
        }}
      >
        {events?.map((e) => (
          <LeaveEvent
            leave={{
              name: (() => {
                const user = users?.find((u) => u.identifier === e.userIdentifier);
                return `${user?.firstName ?? ''} ${user?.lastName ?? ''}`;
              })(),
              startDate: new Date(e.startDate),
              endDate: new Date(e.endDate),
              status: e.status,
              identifier: e.identifier,
              userIdentifier: e.userIdentifier,
            }}
            minStartDate={startDate}
            maxColumns={columns}
            key={e.identifier}
          />
        ))}
      </Box>
      {isOverflow && (
        <Box
          sx={{
            textAlign: 'center',
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            background: 'linear-gradient(0deg, #00000030 0%, transparent 100%)',
            cursor: 'pointer',
          }}
        >
          <Text onClick={() => onShowWeek && onShowWeek(startDate)}>SHOW WEEK</Text>
        </Box>
      )}
    </Box>
  );
}
