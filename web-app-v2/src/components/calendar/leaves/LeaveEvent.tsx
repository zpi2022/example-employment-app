import { Box, DEFAULT_THEME, Text } from '@mantine/core';
import { useModals } from '@mantine/modals';
import React, { useMemo } from 'react';
import { CalendarEventStatus } from '../../../models';
import { getDaysInRangeInclusive } from '../../../utils/DateUtils';
import { Leave } from './Leave';
import { UpdateLeaveDialog } from './UpdateLeaveDialog';

interface CalendarEventProps {
  leave: Leave;
  minStartDate: Date;
  maxColumns: number;
}

export const ColorLookup: Record<CalendarEventStatus, string> = {
  [CalendarEventStatus.ACCEPTED]: DEFAULT_THEME.colors.green[5],
  [CalendarEventStatus.CANCELLED]: DEFAULT_THEME.colors.gray[5],
  [CalendarEventStatus.PENDING]: DEFAULT_THEME.colors.yellow[5],
  [CalendarEventStatus.REJECTED]: DEFAULT_THEME.colors.red[5],
};

export function LeaveEvent({ leave, minStartDate, maxColumns }: CalendarEventProps) {
  const styleProps = useMemo(() => {
    let startColumn: number;
    let duration: number;
    if (leave.startDate > minStartDate) {
      startColumn = getDaysInRangeInclusive(minStartDate, leave.startDate);
      duration = getDaysInRangeInclusive(leave.startDate, leave.endDate);
    } else {
      startColumn = 1;
      duration = getDaysInRangeInclusive(minStartDate, leave.endDate);
    }

    const columnsSpan = Math.min(maxColumns - startColumn + 1, duration);

    return {
      startColumn: startColumn,
      columnsSpan: columnsSpan,
      color: ColorLookup[leave.status],
    };
  }, [leave.startDate, leave.endDate, minStartDate, maxColumns]);

  const modals = useModals();
  const openEditModal = () => {
    const id = modals.openModal({
      title: 'Update event',
      centered: true,
      children: <UpdateLeaveDialog leave={leave} onClose={() => modals.closeModal(id)} />,
      withCloseButton: false,
    });
  };

  return (
    <Box
      onClick={openEditModal}
      p={4}
      pl={8}
      m={4}
      sx={(theme) => ({
        gridColumn: `${styleProps.startColumn} / span ${styleProps.columnsSpan}`,
        borderRadius: theme.radius.md,
        zIndex: 0,
        overflow: 'hidden',
        cursor: 'pointer',
        color: theme.white,
        backgroundColor: styleProps.color,
      })}
    >
      <Text size="xs" weight={500} transform="uppercase" component="div" inline>
        {leave.name}
      </Text>
    </Box>
  );
}
