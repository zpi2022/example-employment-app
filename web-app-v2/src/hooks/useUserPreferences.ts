import { ColorScheme, MantineColor, MANTINE_COLORS } from '@mantine/core';
import { useColorScheme, useLocalStorageValue } from '@mantine/hooks';
import { useEffect } from 'react';

export interface UserPreferences {
  colorScheme: ColorScheme;
  primaryColor: MantineColor;

  toggleColorScheme: () => void;
  setPrimaryColor: (color: MantineColor) => void;
}

export function useUserPreferences(): UserPreferences {
  const preferredColorScheme = useColorScheme();
  const [colorScheme, setColorScheme] = useLocalStorageValue<ColorScheme>({
    key: 'color-scheme',
    defaultValue: preferredColorScheme,
  });
  const [primaryColor, setPrimaryColor] = useLocalStorageValue<MantineColor>({
    key: 'color-primary',
    defaultValue: MANTINE_COLORS[0],
  });

  useEffect(() => {
    console.log('HOOK', primaryColor);
  }, [primaryColor]);

  function toggleColorScheme() {
    setColorScheme(colorScheme === 'dark' ? 'light' : 'dark');
  }
  return {
    colorScheme: colorScheme,
    primaryColor: primaryColor,

    toggleColorScheme: toggleColorScheme,
    setPrimaryColor: setPrimaryColor,
  };
}
