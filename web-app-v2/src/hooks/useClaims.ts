import { useQuery } from 'react-query';
import { createApiFetcher } from '../utils/FetchData';

export interface Claims {
  role: string;
  sub: string;

  [key: string]: string;
}

const config: RequestInit = {
  headers: {
    'X-CSRF': '1',
  },
  credentials: 'same-origin',
};

export function useClaims(): Claims | undefined | null {
  const { data, isError } = useQuery<{ type: string; value: string }[]>(
    'claims',
    createApiFetcher<{ type: string; value: string }[]>({ path: '/bff/user' }, config),
    {
      retry: 2,
    }
  );

  if (isError) return null;
  if (data === undefined) return undefined;

  let claims: Claims = {
    role: '',
    sub: '',
  };

  claims = data.reduce<Claims>((prev, cur) => {
    // eslint-disable-next-line no-param-reassign
    prev[cur.type] = cur.value;
    return prev;
  }, claims);

  return claims;
}
