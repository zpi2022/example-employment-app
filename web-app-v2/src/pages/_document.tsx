import Document from 'next/document';
import { createGetInitialProps } from '@mantine/next';

export default class CustomDocument extends Document {}

CustomDocument.getInitialProps = createGetInitialProps();
