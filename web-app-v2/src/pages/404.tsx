import { Text } from '@mantine/core';

export default function NotFoundPage() {
  return <Text>404 - Page Not Found</Text>;
}
