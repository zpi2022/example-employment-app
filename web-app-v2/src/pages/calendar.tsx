import { Group, Stack } from '@mantine/core';
import { Calendar as MantineCalendar } from '@mantine/dates';
import type { NextPage } from 'next';
import React, { useState } from 'react';
import { Calendar } from '../components/calendar/Calendar';
import { CalendarType } from '../components/calendar/CalendarType';
import { CalendarHeader } from '../components/calendar/elements/CalendarHeader';
import Panel from '../components/common/Panel';
import { addToDate, getToday } from '../utils/DateUtils';

const CalendarPage: NextPage = () => {
  // const api = useApiProvider();

  // console.log(api.getEvents());

  const [today] = useState(getToday);
  const [activeDate, setActiveDate] = useState(today);
  const [calendarType, setCalendarType] = useState(CalendarType.MONTH);

  function changeCalendarPage(changeBy: number): void {
    switch (calendarType) {
      case CalendarType.WEEK:
        setActiveDate(addToDate(activeDate, changeBy, 'weeks'));
        return;
      case CalendarType.MONTH:
        setActiveDate(addToDate(activeDate, changeBy, 'months'));
        // eslint-disable-next-line no-useless-return
        return;
    }
  }

  function handleTodayButton() {
    setActiveDate(today);
  }

  // function handleActiveDateChange(date: Date | null) {
  //   if (date !== null) {
  //     setActiveDate(date);
  //   }
  // }

  return (
    <Group align="stretch" sx={{ width: '100%', height: '100%' }}>
      <Stack align="stretch">
        <Panel contentPadding>
          {/* <Box mb="md">
            <DatePicker value={activeDate} onChange={handleActiveDateChange} />
          </Box>
          <Calendar
            type={CalendarType.MONTH}
            activeDate={activeDate}
            markedDate={today}
            weekdayVariant="narrow"
          /> */}
          <MantineCalendar value={activeDate} onChange={setActiveDate} size="sm" />
        </Panel>
        <Panel flex={1} />
      </Stack>
      <Panel flex={1}>
        <Calendar
          type={calendarType}
          activeDate={activeDate}
          markedDate={today}
          weekdayVariant="long"
          borders
          loadEvents
          onShowWeek={(date) => {
            setActiveDate(date);
            setCalendarType(CalendarType.WEEK);
          }}
          header={
            <CalendarHeader
              activeType={calendarType}
              activeDate={activeDate}
              onNext={() => changeCalendarPage(1)}
              onBefore={() => changeCalendarPage(-1)}
              onToday={handleTodayButton}
              onTypeChange={setCalendarType}
            />
          }
        />
      </Panel>
      {/* <NewLeaveDialog /> */}
    </Group>
  );
};

export default CalendarPage;
