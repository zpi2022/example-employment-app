import { ColorSchemeProvider, MantineProvider } from '@mantine/core';
import { ModalsProvider } from '@mantine/modals';
import { NotificationsProvider } from '@mantine/notifications';
import dayjs from 'dayjs';
import 'dayjs/locale/en-gb';
import 'dayjs/locale/pl';
import weekdayPlugin from 'dayjs/plugin/weekday';
import weekOfYearPlugin from 'dayjs/plugin/weekOfYear';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { Router, useRouter } from 'next/router';
import nProgress from 'nprogress';
import React, { useEffect, useState } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { AuthGuard } from '../components/guards/AuthGuard';
import { MainLayout } from '../components/layout/MainLayout';
import { useUserPreferences } from '../hooks/useUserPreferences';
import { NAVIGATION } from '../navigation';
import { createTheme } from '../theme/theme';

dayjs.extend(weekOfYearPlugin);
dayjs.extend(weekdayPlugin);
Router.events.on('routeChangeStart', nProgress.start);
Router.events.on('routeChangeError', nProgress.done);
Router.events.on('routeChangeComplete', nProgress.done);

export default function AppPage({ Component, pageProps }: AppProps) {
  const [queryClient] = useState(new QueryClient());
  const { colorScheme, primaryColor, toggleColorScheme } = useUserPreferences();
  const router = useRouter();

  useEffect(() => {
    if (router.locale) {
      dayjs.locale(router.locale);
    }
  }, [router.locale]);

  return (
    <>
      <Head>
        <title>Employment app</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <style>{'#__next { height: 100vh; width: 100vw; } '}</style>
      </Head>

      <QueryClientProvider client={queryClient}>
        <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
        <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
          <MantineProvider
            theme={createTheme(colorScheme, primaryColor, router.locale ?? 'en')}
            withNormalizeCSS
            withGlobalStyles
          >
            <NotificationsProvider>
              <ModalsProvider>
                <AuthGuard>
                  <MainLayout navItems={NAVIGATION}>
                    <Component {...pageProps} />
                  </MainLayout>
                </AuthGuard>
              </ModalsProvider>
            </NotificationsProvider>
          </MantineProvider>
        </ColorSchemeProvider>
      </QueryClientProvider>
    </>
  );
}
