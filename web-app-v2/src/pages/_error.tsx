import { Box } from '@mantine/core';
import type { ErrorProps } from 'next/error';
import NextError from 'next/error';
import React from 'react';

export default function ErrorPage(props: ErrorProps) {
  return (
    <Box
      sx={{
        height: '100%',
        '&>*': {
          height: '100%!important',
        },
      }}
    >
      <NextError {...props} />
    </Box>
  );
}
