/* tslint:disable */
/* eslint-disable */
/**
 * Employment API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import {
  CalendarEventStatus,
  CalendarEventStatusFromJSON,
  CalendarEventStatusToJSON,
} from './CalendarEventStatus';

/**
 *
 * @export
 * @interface UpdateCalendarEventCommand
 */
export interface UpdateCalendarEventCommand {
  /**
   *
   * @type {number}
   * @memberof UpdateCalendarEventCommand
   */
  userIdentifier: number;
  /**
   *
   * @type {Date}
   * @memberof UpdateCalendarEventCommand
   */
  startDate: Date;
  /**
   *
   * @type {Date}
   * @memberof UpdateCalendarEventCommand
   */
  endDate: Date;
  /**
   *
   * @type {CalendarEventStatus}
   * @memberof UpdateCalendarEventCommand
   */
  status: CalendarEventStatus;
}

export function UpdateCalendarEventCommandFromJSON(json: any): UpdateCalendarEventCommand {
  return UpdateCalendarEventCommandFromJSONTyped(json, false);
}

export function UpdateCalendarEventCommandFromJSONTyped(
  json: any,
  ignoreDiscriminator: boolean
): UpdateCalendarEventCommand {
  if (json === undefined || json === null) {
    return json;
  }
  return {
    userIdentifier: json['userIdentifier'],
    startDate: new Date(json['startDate']),
    endDate: new Date(json['endDate']),
    status: CalendarEventStatusFromJSON(json['status']),
  };
}

export function UpdateCalendarEventCommandToJSON(value?: UpdateCalendarEventCommand | null): any {
  if (value === undefined) {
    return undefined;
  }
  if (value === null) {
    return null;
  }
  return {
    userIdentifier: value.userIdentifier,
    startDate: value.startDate.toISOString().substr(0, 10),
    endDate: value.endDate.toISOString().substr(0, 10),
    status: CalendarEventStatusToJSON(value.status),
  };
}
