/* tslint:disable */
/* eslint-disable */
/**
 * Employment API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists } from '../utils/FetchData';

/**
 *
 * @export
 * @interface HttpValidationProblemDetails
 */
export interface HttpValidationProblemDetails {
  [key: string]: any | any;
  /**
   *
   * @type {{ [key: string]: Array<string> | undefined; }}
   * @memberof HttpValidationProblemDetails
   */
  errors?: { [key: string]: Array<string> | undefined } | null;
  /**
   *
   * @type {string}
   * @memberof HttpValidationProblemDetails
   */
  type?: string | null;
  /**
   *
   * @type {string}
   * @memberof HttpValidationProblemDetails
   */
  title?: string | null;
  /**
   *
   * @type {number}
   * @memberof HttpValidationProblemDetails
   */
  status?: number | null;
  /**
   *
   * @type {string}
   * @memberof HttpValidationProblemDetails
   */
  detail?: string | null;
  /**
   *
   * @type {string}
   * @memberof HttpValidationProblemDetails
   */
  instance?: string | null;
}

export function HttpValidationProblemDetailsFromJSON(json: any): HttpValidationProblemDetails {
  return HttpValidationProblemDetailsFromJSONTyped(json, false);
}

export function HttpValidationProblemDetailsFromJSONTyped(
  json: any,
  ignoreDiscriminator: boolean
): HttpValidationProblemDetails {
  if (json === undefined || json === null) {
    return json;
  }
  return {
    ...json,
    errors: !exists(json, 'errors') ? undefined : json['errors'],
    type: !exists(json, 'type') ? undefined : json['type'],
    title: !exists(json, 'title') ? undefined : json['title'],
    status: !exists(json, 'status') ? undefined : json['status'],
    detail: !exists(json, 'detail') ? undefined : json['detail'],
    instance: !exists(json, 'instance') ? undefined : json['instance'],
  };
}

export function HttpValidationProblemDetailsToJSON(
  value?: HttpValidationProblemDetails | null
): any {
  if (value === undefined) {
    return undefined;
  }
  if (value === null) {
    return null;
  }
  return {
    ...value,
    errors: value.errors,
    type: value.type,
    title: value.title,
    status: value.status,
    detail: value.detail,
    instance: value.instance,
  };
}
