import { MANTINE_COLORS } from '@mantine/core';
import { MdCalendarToday, MdDarkMode } from 'react-icons/md';
import { NavigationItem } from './components/layout/Sidebar';

const COLORS = MANTINE_COLORS.slice(2);

export const NAVIGATION: NavigationItem[] = [
  {
    name: 'Calendar',
    href: '/calendar',
    icon: <MdCalendarToday />,
    color: 'orange',
  },
  ...COLORS.map((color) => ({
    name: `Page ${color}`,
    href: color,
    icon: <MdDarkMode />,
    color: color,
  })),
];
