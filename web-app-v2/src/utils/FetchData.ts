const basePath = '';

export type Json = any;
export type HTTPHeaders = { [key: string]: string };
export type HTTPQuery = {
  [key: string]:
    | string
    | number
    | null
    | boolean
    | Array<string | number | null | boolean>
    | HTTPQuery;
};
export type HTTPBody = Json | FormData | URLSearchParams;
export enum HTTPMethod {
  POST = 'POST',
  GET = 'GET',
  PUT = 'PUT',
  DELETE = 'DELETE',
  PATCH = 'PATCH',
}

export interface FetchParams {
  url: string;
  init: RequestInit;
}

export interface RequestOpts {
  path: string;
  method?: HTTPMethod;
  headers?: HTTPHeaders;
  query?: HTTPQuery;
  body?: HTTPBody;
}

function queryParamsStringify(params: HTTPQuery, prefix: string = ''): string {
  return Object.keys(params)
    .map((key) => {
      const fullKey = prefix + (prefix.length ? `[${key}]` : key);
      const value = params[key];
      if (value == null) {
        return '';
      }
      if (value instanceof Array) {
        const multiValue = value
          .map((singleValue) => encodeURIComponent(String(singleValue)))
          .join(`&${encodeURIComponent(fullKey)}=`);
        return `${encodeURIComponent(fullKey)}=${multiValue}`;
      }
      if (value instanceof Date) {
        return `${encodeURIComponent(fullKey)}=${encodeURIComponent(value.toISOString())}`;
      }
      if (value instanceof Object) {
        return queryParamsStringify(value, fullKey);
      }
      return `${encodeURIComponent(fullKey)}=${encodeURIComponent(String(value))}`;
    })
    .filter((part) => part.length > 0)
    .join('&');
}

const isBlob = (value: any) => typeof Blob !== 'undefined' && value instanceof Blob;

function createFetchParams(context: RequestOpts, initOverrides?: RequestInit) {
  let url = basePath + context.path;
  if (context.query !== undefined && Object.keys(context.query).length !== 0) {
    // only add the querystring to the URL if there are query parameters.
    // is done to avoid urls ending with a "?" character which buggy webservers
    // do not handle correctly sometimes.
    url += `?${queryParamsStringify(context.query)}`;
  }
  const body: HTTPBody =
    (typeof FormData !== 'undefined' && context.body instanceof FormData) ||
    context.body instanceof URLSearchParams ||
    isBlob(context.body)
      ? context.body
      : JSON.stringify(context.body);

  const headers: HTTPHeaders = {
    'content-type': 'application/json; charset=utf-8',
    ...context.headers,
  };
  const init: RequestInit = {
    credentials: 'same-origin',
    method: context.method ?? HTTPMethod.GET,
    headers: headers,
    body,
    ...initOverrides,
  };
  return { url, init };
}

export async function fetchFromApi<TResponse>(
  context: RequestOpts,
  initOverrides?: RequestInit
): Promise<TResponse> {
  const { url, init } = createFetchParams(context, initOverrides);
  const response = await fetch(url, init);
  if (response.status === 200) {
    return response.json();
  }

  if (response.status === 204) {
    return [] as unknown as TResponse;
  }

  throw await response.text();
}

export function createApiFetcher<TResponse>(
  context: RequestOpts,
  initOverrides?: RequestInit
): () => Promise<TResponse> {
  return async () => fetchFromApi(context, initOverrides);
}

export function exists(json: any, key: string) {
  const value = json[key];
  return value !== null && value !== undefined;
}

export function mapValues(data: any, fn: (item: any) => any) {
  return Object.keys(data).reduce((acc, key) => ({ ...acc, [key]: fn(data[key]) }), {});
}
