import dayjs from 'dayjs';

export const CALENDAR_COLUMNS: number = 7;

export type MonthNameVariant = '2-digit' | 'long' | 'narrow' | 'numeric' | 'short';
export type WeekdayNameVariant = 'long' | 'narrow' | 'short';
export type Unit = 'days' | 'weeks' | 'months';

export function getToday(): Date {
  return dayjs().toDate();
}

export function getIsoStringDateOnly(date: Date): string {
  return dayjs(date).format('YYYY-MM-DD').substr(0, 10);
}

export function getWeekdayName(date: Date, type: WeekdayNameVariant): string {
  return date.toLocaleString(dayjs.locale(), { weekday: type });
}

export function getMonthName(date: Date, type: MonthNameVariant): string {
  return date.toLocaleString(dayjs.locale(), { month: type });
}

export function getMonthNameByNumber(month: number, type: MonthNameVariant): string {
  return dayjs().set('month', month).toDate().toLocaleString(dayjs.locale(), { month: type });
}

export function getFirstDayOfMonth(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth(), 1);
}

export function getLastDayOfMonth(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}

export function isSameFullDate(st?: Date, nd?: Date): boolean {
  return (
    st?.getFullYear() === nd?.getFullYear() &&
    st?.getMonth() === nd?.getMonth() &&
    st?.getDate() === nd?.getDate()
  );
}

export function isSameMonth(st?: Date, nd?: Date): boolean {
  return st?.getMonth() === nd?.getMonth();
}

export function getDaysInRangeInclusive(from: Date, to: Date): number {
  // console.log('From', from.toISOString(), 'To', to.toISOString());
  // if (to < from) {
  //   throw Error(`Right boundary (to - ${to.toISOString()})  should be bigger than left (from - ${from.toISOString()}) `);
  // }
  return dayjs(to).diff(from, 'days') + 1;
}

export function addToDate(date: Date, amount: number, unit: Unit): Date {
  return dayjs(date).add(amount, unit).toDate();
}

export function asUTC(date: Date): Date {
  return new Date(
    Date.UTC(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes(),
      date.getSeconds(),
      date.getMilliseconds()
    )
  );
}
