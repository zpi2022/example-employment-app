# Aplikacja do zarządzania pracownikami

## Założenia

- Możliwość logowania się, tworzenia kont pracownikom
- Możliwość przeglądania informacji o pracownikach
- Możliwość tworzenia hierarchii pracowniczej (tworzenie zespołów) i role użytkowników
- Możliwość zgłaszania urlopów przez pracowników (przełożony może zaakceptować lub odrzucić zgłoszenie)
- Możliwość przeglądaia kalendarza urlopów (pracownika oraz jego podwładnych) i dni wolnych od pracy
- Możliwość dodawania zadań dla zespołów, pracowników

## Wymagania funkcjonalne i niefunkcjonalne

### Funkcjonalne

- System posiada poziomy dostępu (manager, team leader, employee)
- Pracownik może zalogwać się
- Pracownik może zgłosić chęć wzięcia urlopu
- Pracownik może wyświetlać kaledarz ze swoimi zgłoszeniami i dniami wolnymi
- Pracownik może przeglądać przypisane do niego zadania
- Pracownik może zmieniać status swojego zadania
- Pracownik może sprawdzać historię statusów swojego zadania
- Lider zespołu może akceptować/odrzucać złgoszenia urlopów członków zespołu
- Lider zespołu może dodawać zadania dla zespołu
- Lider zespołu może dodawać zadania członkom zespołu
- Lider zespołu może edytować zadania w zespole
- Lider zespołu może przeglądać dane członków zespołu
- Menedżer może tworzyć nowych pracowników (tworzyć nowe konta)
- Menedżer może zmieniać uprawnienia pracowników
- Menedżer może edytować dane pracowników
- Menedżer może dodawać nowe zespoły
- Menedżer może przypisywać pracowników do zespołów
- Menedżer moze akceptować/odrzucać złgoszenia urlopów liderów zespołów
- Menedżer może dodawać zadania dla zaspołów, pracowników
- Menedżer może edytować zadania
- Menedżer może przeglądać dane wszystkich pracowników

### Niefunkcjonalne

- System działa w czasie rzeczywistym
- Aplikacja wymaga stałego dostępu do internetu
- Aplikacja podzielona jest na serwis WWW i aplikację interfejs programowania aplikacji (API)
- Aplikacja działa na najnowszych wersjach przeglądarek (chrome, firefox)
- Aplikacja jest przystosowana do ekranów komputerów, tabletów i telefonów
- Aplikacja powinna pozwalać na dalszą rozbudowę

## Przy użyciu

- TypeScript
- ReactJS
- NextJS
- MUI 5
- React Query
- .Net 6.0 (architektura heksagonalna)
- PostgreSQL
- Swagger
- Swashbuckle
- IdentityServer 5

## Terminy

- 21.10.2021 - Stworzony projekt, pierwszy prototyp UI
- 28.10.2021 - API i komunikacja
- 04.11.2021 - Uwierzytelnianie i autoryzacja
- 18.11.2021 - Moduł pracowników
- 25.11.2021 - Moduł kalendarza
- 02.12.2021 - Moduł kalendarza c.d.
- 09.12.2021 - Moduł zadań
- 16.12.2021 - Moduł zadań c.d.
- Styczeń i dalej - dodatkowe zmiany na podstawie opinii, deployment
