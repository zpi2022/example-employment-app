using Duende.Bff.EntityFramework;
using Duende.Bff.Yarp;

using Gateway.Web;

using IdentityModel;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Serilog;

using System;
using System.Security.Claims;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .Enrich.FromLogContext()
    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{NewLine}\t{Exception}{NewLine}", theme: Serilog.Sinks.SystemConsole.Themes.AnsiConsoleTheme.Code)
    .CreateBootstrapLogger();

Log.Information("Starting up");

try {
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((context, services, configuration) =>
        configuration
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{NewLine}\t{Exception}{NewLine}", theme: Serilog.Sinks.SystemConsole.Themes.AnsiConsoleTheme.Code)
            .ReadFrom.Services(services)
            .ReadFrom.Configuration(context.Configuration)
        );
    builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
        config.AddJsonFile("appsettings.local.json", optional: true, reloadOnChange: true)
    );

    builder.Services.AddControllers();

    var dbConfig = builder.Configuration.GetRequiredSection(ConfigPaths.DatabaseSection).Get<DatabaseConfiguration>();
    builder.Services.AddBff()
        .AddServerSideSessions()
        // .AddEntityFrameworkServerSideSessions(options => {
        //     options.UseNpgsql(dbConfig.ConnectionString, npgsqlOptions => {
        //         npgsqlOptions.MigrationsHistoryTable(dbConfig.MigrationsTable, "gateway");
        //         npgsqlOptions.SetPostgresVersion(dbConfig.DatabaseVersion);
        //         npgsqlOptions.MigrationsAssembly("Duende.BFF.EntityFramework");
        //     });
        // })
        .AddRemoteApis();


    builder.Services.AddReverseProxy()
        .AddTransforms<AccessTokenTransformProvider>()
        .LoadFromConfig(builder.Configuration.GetSection(ConfigPaths.ReverseProxySection));

    builder.Services.AddUserAccessTokenHttpClient("client-spa", configureClient: client => {
        client.BaseAddress = new Uri("http://localhost:3000/");
    });

    builder.Services.AddAuthentication(options => {
        options.DefaultScheme = "cookie";
        options.DefaultChallengeScheme = "oidc";
        options.DefaultSignOutScheme = "oidc";
    })
        .AddCookie("cookie", options => {
            options.Cookie.Name = "__Host-bff";
            options.Cookie.SameSite = SameSiteMode.Strict;
        })
        .AddOpenIdConnect("oidc", options => {
            options.Authority = "https://localhost:5000";
            options.ClientId = "gateway-spa";
            options.ClientSecret = "gateway-spa";
            options.ResponseType = "code";
            options.ResponseMode = "query";

            options.GetClaimsFromUserInfoEndpoint = true;
            options.MapInboundClaims = false;
            options.SaveTokens = true;

            options.Scope.Clear();
            options.Scope.Add("openid");
            options.Scope.Add("profile");
            options.Scope.Add("email");
            options.Scope.Add("offline_access");
            options.Scope.Add("users");
            options.Scope.Add("roles");

            options.ClaimActions.MapJsonKey(JwtClaimTypes.Role, JwtClaimTypes.Role, "string");

            options.TokenValidationParameters = new() {
                NameClaimType = "name",
                RoleClaimType = "role"
            };
        });

    var app = builder.Build();

    if (!app.Environment.IsDevelopment()) {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
    }
    app.UseSerilogRequestLogging();
    app.UseHttpsRedirection();

    app.UseRouting();

    app.UseAuthentication();
    app.UseBff();
    app.UseAuthorization();

    app.UseEndpoints(endpoints => {
        endpoints.MapBffManagementEndpoints();

        endpoints.MapReverseProxy()
            .AsBffApiEndpoint(false);
    });

    app.Run();
} catch (Exception ex) {
    Log.Fatal(ex, "Unhandled exception");
} finally {
    Log.Information("Shut down complete");
    Log.CloseAndFlush();
}