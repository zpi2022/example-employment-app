namespace Gateway.Web {
    internal static class ConfigPaths {
        internal const string Serilog = "SerilogLogging";

        internal const string CorsPolicy = "CorsPolicy";

        internal const string DatabaseSection = "Database";

        internal const string ReverseProxySection = "ReverseProxy";
    }
}